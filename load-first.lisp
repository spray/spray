;; Steven's init file
;; Mainly ASDF stuff that I will always need.

;; Load ASDF
(load "asdf/asdf.lisp")

;; Let ASDF know where our systems are
(dolist (dir '(
	       ;; You could add more directories here..
	       ;; or just add symlinks to ASD files in one of these dir's.
	       "steven/"
	       "steven/raytracer/"
	       "imago/src/"
	       "zlib/src/"
	       ))
  (push dir asdf:*central-registry*))

;; Helper function to load systems
(defun asdf-load (compile? &rest system-names)
  (when (eql system-names nil) (format t "No system names were given!"))
  (dolist (system-name system-names)
	(cond (compile?
			(asdf:oos 'asdf:load-op system-name))
		  (t
			(asdf:oos 'asdf:load-source-op system-name)))))

;; Load systems and use pkgs we frequently use
(asdf-load t :steven)
(use-package :an.steven.debug)

;; Define some useful things
(defalias in-pkg in-package)
(defalias use-pkg use-package)

