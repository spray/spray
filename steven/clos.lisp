(in-package :an.steven.clos)

(defmacro defclass-java-style (name base-classes &rest field-forms)
  (let ((field-decls '()))
	;; Build the field decls
	(dolist (field-form field-forms)
		(push
		  ;; field-decl-java-style expects expanded args, like (fdjs kind name etc. etc.)
		  ;; So we gotta use apply
		  (apply #'field-decl-java-style field-form)
		  field-decls))
	;; Now build the whole defclass
	`(defclass ,name ,base-classes ,field-decls)))

(defun field-decl-java-style (kind name initarg initform &rest other)
  "Only meant for use by defclass-java-style!  This won't work inside a normal defclass!"
  `(,name :initarg ,initarg
		  :initform ,initform
		  ;; Define an accessor/reader with the same name
		  ,(switch kind
				   ((:public :accessor)
					(:readonly :reader))
				   (error "incorrect field kind!  Should be :public or :readonly"))
		  ,name
		  ,@other))


;; EXAMPLES
'(defclass-java-style vehicle ())
'(defclass-java-style boat (vehicle)
					  (:public engine :engine nil)
					  (:public length :length 100)
					  (:readonly is-on :is-on nil))

;; More helpers

(defmacro define-typical-print-method (class-name (object stream) &body body)
  `(defmethod print-object ((,object ,class-name) ,stream)
    (print-unreadable-object (,object ,stream :type t :identity t)
      ,@body)))

(defmacro define-formatting-print-method (class-name object fmt-string &rest fmt-args)
  `(define-typical-print-method ,class-name (,object strm)
    (format strm ,fmt-string ,@fmt-args)))

;; EXAMPLES
'(define-typical-print-method primitive-closed-convex-model (obj strm)
  (format strm "primitive: ~a" (primitive obj)))

'(define-formatting-print-method
    primitive-closed-convex-model obj
  "primitive: ~a" (primitive obj))
    
