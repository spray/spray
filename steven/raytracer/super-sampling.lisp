(in-package :an.steven.raytracer)

(defun sum-of-colors (colors)
  (cond ((eql nil colors) (make-vector 0 0 0))
		(t (v+ (car colors) (sum-of-colors (cdr colors))))))

(defun average-of-colors (colors)
  (cond ((eql colors nil) (make-vector 0 0 0))
		(t (nv/ (sum-of-colors colors)
				(length colors)))))

(defun do-grid (cx cy half-edge half-divs fn)
  (cond ((= half-divs 0) (funcall fn cx cy))
		(t
		  (let ((sx (- cx half-edge))	;; left/bottom of grid
				(sy (- cy half-edge))
				(dx (/ half-edge half-divs))	;; Width/height of a grid box
				(dy (/ half-edge half-divs))
				(num-divs (* 2 half-divs)))
			;; Calculate points
			(loop for ix from 0 to num-divs do
				  (loop for iy from 0 to num-divs collect
						(funcall fn
								 (+ sx (* ix dx))
								 (+ sy (* iy dy)))))))))

(defun raytrace-pixel-super-sampled (x y w h scene view ss-level)
  (let ((samples '()))
	(do-grid x y 0.5 ss-level
			 #'(lambda (x y)
				 ;; Shoot the ray for this screen pt
				 ;; and add the color to the samples list
				 (push (raytrace-screen-point x y w h scene view) samples)))
	;; Now average samples and return
	(average-of-colors samples)))
