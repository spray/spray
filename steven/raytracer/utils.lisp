(in-package :an.steven.raytracer)

;; TODO - may wanna rename these..but incident is pretty OK
;; Although..incident dir should be the ray direction flipped..so it's not quite right.

(defun outward-dir? (dir surface-normal)
  "This returns true if the given direction seems to be going out of the hit surface.  It simply compares the direction of the hit to the surface normal."
  (> (dot-product dir surface-normal) 0.0))

(defun inward-dir? (dir surface-normal)
  "Similar to above"
  (< (dot-product dir surface-normal) 0.0))

(defun tangential-dir? (dir surface-normal)
  "Similar to above"
  (= (dot-product dir surface-normal) 0.0))

