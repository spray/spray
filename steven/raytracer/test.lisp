(in-package :an.steven.raytracer)

;; Test data

(defvar *shiney-red-mat*
  (make-instance 'phong-material
				 :diffuse-map (make-solid-map (make-vector 1 0 0))
				 :specular-color (make-vector 0.1 0.1 0.1)
				 :specular-exponent 2))
(defvar *diffuse-green-mat*
  (make-instance 'diffuse-phong-material
				 :diffuse-map (make-solid-map (make-vector 0 1 0))
				 :specular-color (make-vector 0 0 0)
				 :specular-exponent 0))
(defvar *shiney-green-mat*
  (make-instance 'phong-material
				 :diffuse-map (make-solid-map (make-vector 0 1 0))
				 :specular-color (make-vector 0.1 0.1 0.1)
				 :specular-exponent 2))
(defvar *shiney-white-mat*
  (make-instance 'phong-material
				 :diffuse-map (make-solid-map (make-vector 1 1 1))
				 :specular-color (make-vector 0.1 0.1 0.1)
				 :specular-exponent 2))

(defvar *green-mat*
  (make-instance 'phong-material
				 :diffuse-map (make-solid-map (make-vector 0 1 0))
				 :specular-color (make-vector 0 0 0)
				 :specular-exponent 2))
(defvar *blue-yellow-chex* (make-instance 'checkerboard-texture :color1 #(0 0 1) :color2 #(1 1 0)))
(defvar *red-green-chex* (make-instance 'checkerboard-texture :color1 #(1 0 0) :color2 #(0 1 0)))
(defvar *chex*
  (make-instance 'phong-material
				 :diffuse-map *blue-yellow-chex*
				 :specular-color (make-vector 1 1 1)
				 :specular-exponent 2))
(defvar *chex2*
  (make-instance 'phong-material
				 :diffuse-map *red-green-chex*
				 :specular-color (make-vector 1 1 1)
				 :specular-exponent 2))

(defvar *diffuse-chex*
  (make-instance 'diffuse-phong-material
                 :diffuse-map *blue-yellow-chex*
                 :specular-color #(0 0 0)
                 :specular-exponent 0))
(defvar *diffuse-chex2*
  (make-instance 'diffuse-phong-material
                 :diffuse-map *red-green-chex*
                 :specular-color #(0 0 0)
                 :specular-exponent 0))

(defun make-png-texture (png-file) (make-instance 'image-texture :img (read-png png-file)))
(defun make-diffuse-png-texture (png-file)
  (make-instance 'diffuse-phong-material
		 :diffuse-map (make-png-texture png-file)
		 :specular-color (make-vector 0 0 0)
		 :specular-exponent 0))

(defun make-shiney-png-texture (png-file)
  (make-instance 'phong-material
		 :diffuse-map (make-png-texture png-file)
		 :specular-color (make-vector 1 1 1)
		 :specular-exponent 2))

(defvar *hardwood-mat* (make-shiney-png-texture "floorboards1small.png"))
(setf (reflectivity *hardwood-mat*) 0.25)

;; (defvar *rocks-mat* (make-diffuse-png-texture "rocks.png"))
(defvar *bricks-mat* (make-diffuse-png-texture "bricks.png"))
;; (defvar *grass-mat* (make-diffuse-png-texture "greengrass.png"))

(defvar *test-view* (make-instance 'simple-RT-view))
(defvar *test-scene-01*
  (make-instance 'scene
		 :ambient-light (make-vector 0.1 0.1 0.1)
		 :background-color (make-vector 1 1 1)
		 :model (make-instance
			 'composite-model
			 :models
			 (list
			  (make-sphere-model (make-vector 0 2 0) 2 *glass-material*)
			  (make-sphere-model (make-vector 0 2 -5) 1 *glass-material*)
			  (make-sphere-model (make-vector 1 -2 0) 1 *glass-material*)
			  (make-sphere-model (make-vector 0 0 -1050) 1000 *glass-material*)
			  ))
		 :lights (list (make-instance 'point-light
					      :origin (make-vector 1 1 5)
					      :color (make-vector 0.8 0.3 0.3))
			       (make-instance 'point-light
					      :origin (make-vector -1 -1 5)
					      :color (make-vector 0.3 0.3 0.8)))
		 ))

(defvar *debug-sphere-scene*
  (make-instance 'scene
		 :ambient-light #(0.1 0.1 0.1)
		 :background-color #(0 0 0)
		 :model (make-instance
			 'composite-model
			 :models
			 (list
;			  (make-sphere-model #(0 0 -3) 3 *shiney-red-mat*)
			  (make-box-model #(-3 -3 -6) #(3 3 0) *shiney-red-mat*)
			  ))
		 
		 :lights (list 
			  (make-instance 'point-light :origin (make-vector 0 0 -8) :color (make-vector 1 1 1))
			  )
		 ;; :empty-space-medium (make-instance 'linear-fog)
		 ))

(defvar *debug-xmission-scene*
  (make-instance 'scene
		 ;; :ambient-light #(1 0.1 0.1)
		 :background-color #(0 0 0)
		 :model (make-instance
			 'composite-model
			 :models
			 (list
			  ;; (make-box-model #(-3 -1 -5) #(3 3 -4) *glass-material*)
			  (make-sphere-model #(3 -3 -5) 3 *water-material*)	;; middle ball
			  (make-sphere-model #(10 4 -20) 6 *shiney-red-mat*)	;; right red ball
			  (make-sphere-model #(-5 -2 -12) 4 *glass-material*)	;; left glass

			  (make-sphere-model #(-4 8 -20) 6 *chex*) ;; shiney textured sphere

			  (make-instance 'xz-plane
					 :dist -6
					 :material *hardwood-mat*
					 :uv-map (make-instance 'xz-plane-uv-map
								:uv-scale 0.15))
			  (make-instance 'xy-plane
					 :dist -30
					; :material *grass-mat*
					 :material *bricks-mat*
					 :uv-map (make-instance 'xy-plane-uv-map :uv-scale 0.05))
			  ))
		 
		 :lights (list 
			  (make-instance 'point-light :origin (make-vector 5 8 0) :color (make-vector 0.5 0.5 1))
			  (make-instance 'point-light :origin (make-vector -5 8 -12) :color (make-vector 1 0.5 0.5))
			  )
		 ;; :empty-space-medium (make-instance 'linear-fog)
		 ))

(defvar *cool-glass-scene*
  (make-instance 'scene
		 :model (make-instance
			 'composite-model
			 :models
			 (list
			  (make-sphere-model #(0 -2 -6) 4 *glass-material*)
					;(make-sphere-model #(0 -1 -6) 2 *shiney-red-mat*)
			  (make-instance 'xz-plane
					 :dist -6
					 :material *chex*
					 :uv-map (make-instance 'xz-plane-uv-map :uv-scale 0.5))
					;(make-instance 'xy-plane
					;:dist -20
					;:material *shiney-red-mat*
					;:uv-map (make-instance 'xy-plane-uv-map :uv-scale 0.125))
			  ))
		 :lights (list (make-instance 'point-light
					      :origin #(6 3 5)
					      :color (make-vector 1 1 1)))
		 ))

;; The test runner
(defun rt-test ()
  (let ((*image-edge-len* 200)
	(*depth-limit* 9)
	(*super-sampling-level* 0)
;;	(*scene* *light-behind-glass-coming-thru-bug-scene*)
	(*scene* *debug-xmission-scene*)
	(*scene-monkey-output* nil))

    ;; Print the scene out to a scene-monkey file
    (print-to-scene-monkey *scene*)
    (dbl *scene-monkey-output*)

    ;; Raytrace a full render
    (raytrace-scene-to-tga *scene*
			   "0001.tga"
			   *image-edge-len* *image-edge-len*
			   *test-view*)

    ;; Pixel debugging 
    (let ((*scene-monkey-output* t)
	  (*trace-output* *standard-output*))
      (defun my-raytrace-screen-point (x y &key (y-is-img-coord nil))
	(raytrace-screen-point x (if y-is-img-coord
				     (img-y->world-y y)
				     y)
			       *image-edge-len* *image-edge-len*
			       *scene* *test-view*))
      ;; Trace rays spawned
      '(trace
	get-ray-illum
	get-ray-hit
	get-direct-illums
	illumination-from
	secondary-shots
       )

      ;; Trace spherical uv
      '(trace spherical-uv
	     sin
	     ^2)
      '(my-raytrace-screen-point 64 182)
      
      '(format *trace-output* "a point on the sphere ========== ~%")
      '(raytrace-screen-point 90 (img-y->world-y 90 *image-edge-len*)
	*image-edge-len* *image-edge-len*
	*scene* *test-view*)
      '(format *trace-output* "the point that causes the complex number =========== ~%")
      '(raytrace-screen-point 51 21
	*image-edge-len* *image-edge-len*
	*scene* *test-view*))))

;; Unit tests

;; TESTING

(defun TEST ()
  (blk "testing lights..")
  ;; make a light
  (let ((lite (make-instance 'point-light
							 :origin (make-vector 0 1 0)
							 :color (make-vector 1 1 1)))
		(hit (make-instance 'hit
							:t-val 0
							:shot (make-instance 'shot :ray (make-ray-from-line #(0 0 0) #(1 1 1)))
							:normal #(0 1 0)
							:material *glass-material*
							:basic-model (make-sphere-model (make-vector 0 0 -1050) 1000 *glass-material*)
							)))
	(blk "getting illumination")
	(dbl (illumination-from lite hit *empty-scene*))))

(defun TEST-refraction ()
  )

(defun TEST-tmap ()
  (let ((chex (make-instance 'checkerboard-texture)))
	(dbl (get-color chex 0 0))
	(dbl (get-color chex 0.25 0))
	(dbl (get-color chex 0.75 0))
	(dbl (get-color chex 0 0.25))
	(dbl (get-color chex 0 0.75))
  ))
