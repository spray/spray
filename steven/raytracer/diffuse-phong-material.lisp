(in-package :an.steven.raytracer)

;; Phong shaded diffuse-only materials.  Meaning, no reflected/transmitted rays

(defclass-java-style
  diffuse-phong-material (phong-material)
  ;; Color - purple-ish by default
  (:public color :color #(1.0 0.5 1.0)))

(defmethod secondary-shots ((material diffuse-phong-material) hit)
  "For diffuse materials, there are no secondary shots"
  ())
