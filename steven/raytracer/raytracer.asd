
(asdf:defsystem
  raytracer
  :author "Steven An"
  :version "1.0"

  :components
  ((:file "packages")

   (:file "hit" :depends-on ("packages"))
   (:file "illum" :depends-on ("packages"))
   (:file "shot" :depends-on ("packages"))


   (:file "view" :depends-on ("packages"))
   (:file "imago-utils" :depends-on ("packages"))
   (:file "utils" :depends-on ("packages"))

   (:file "primitive" :depends-on ("packages"))
   (:file "box-primitive" :depends-on ("primitive"))

   (:file "light" :depends-on ("illum"))
   (:file "medium" :depends-on ("packages"))
   (:file "model" :depends-on ("packages"))

   (:file "shading" :depends-on ("packages"))
   (:file "texture" :depends-on ("packages"))
   (:file "image-texture" :depends-on ("packages" "texture"))

   (:file "uv-map" :depends-on ("packages"))
   (:file "spherical-uv-map" :depends-on ("uv-map"))

   (:file "material" :depends-on ("utils" "illum" "medium" "texture" "uv-map"))
   (:file "diffuse-phong-material" :depends-on ("material" "utils" "illum" "medium" "texture" "uv-map"))
   (:file "fresnel-material" :depends-on ("utils" "illum" "medium" "texture" "uv-map" "material"))

   (:file "super-sampling" :depends-on ("packages"))
   (:file "trace-filter" :depends-on ("hit"))

   (:file "scene" :depends-on ("model"))
   (:file "main" :depends-on ("packages"))
   (:file "test" :depends-on ("main" "scene" "material" "shading" "model" "view" "shot" "texture" "uv-map"))

   (:file "scene-monkey" :depends-on ("model" "scene" "shot" "packages"))
   )

  :depends-on
  (
   :steven
   :imago
   )
  )

