(in-package :an.steven.raytracer)

;; Scenes

(defclass-java-style scene-base ()
  (:public ambient-light :ambient-light (make-vector 0.05 0.05 0.05))
  (:public background-color :background-color (make-vector 0 0 0))
  (:public empty-space-medium :empty-space-medium (make-instance 'boring-medium))
  )
(defgeneric get-ray-hit (scene shot))
(defgeneric get-direct-illums (scene hit))	;; Usually, it's from light objects

;; Scenes with light objects, as opposed to predetermined illumination

(defclass-java-style lighted-scene (scene-base)
  (:public lights :lights '()))
(defmethod get-direct-illums ((scene lighted-scene) hit)
  (loop for light in (lights scene)
	collect (illumination-from light hit scene)))

;; A scene with just a model.  This is the most common kind, so it's just a "scene"
(defclass-java-style scene (lighted-scene)
  (:public model :model nil))

(defmethod get-ray-hit ((scene scene) shot)
  (let ((hit (get-ray-hit (model scene) shot)))
    (if (non-nil? hit)
      (print-to-scene-monkey hit)
      (print-to-scene-monkey shot))
    hit))

