(in-package :an.steven.raytracer)

(defclass-java-style
    box-primitive (primitive-base)
  (:public mins :mins (error "No mins given!"))
  (:public maxs :maxs (error "No maxs given!")))

(defconstant +epsilon+ 1e-9);; this should be 1e-9 ideally

(defun epsilon-if-zero (x)
  (if (= x 0.0)
      +epsilon+
      x))

(defun only-non-negatives (&rest l)
  (let ((out '()))
    (dolist (x (reverse l))
      (when (not (< x 0.0))
	(push x out)))
    out))

'(defmethod ray-intx-t-vals ((prim box-primitive) ray)
  "Based on Revelles's octree traversal method"
  (let ((r (copy-ray ray)))
    ;; Adjust the ray's origin and direction
    (dolist (i (list 0 1 2))
      (if (= 0.0 (aref (r-dir r) i))
	  (setf (aref (r-dir r) i) +epsilon+)
	  ;; else
	  (if (< (aref (r-dir r) i) 0.0)
	      (progn (setf (aref (r-origin r) i)
			   (- (+ (aref (mins prim) i)
				 (aref (maxs prim) i))
			      (aref (r-origin r) i)))
		     (setf (aref (r-dir r) i)
			   (* -1 (aref (r-dir r) i)))))))
    (let ((xmin (v-x (mins prim))) (xmax (v-x (maxs prim)))
	  (ymin (v-y (mins prim))) (ymax (v-y (maxs prim)))
	  (zmin (v-z (mins prim))) (zmax (v-z (maxs prim))))
      (let ((tx0 (/ (- xmin (v-x (r-origin r))) (v-x (r-dir r))))
	    (tx1 (/ (- xmax (v-x (r-origin r))) (v-x (r-dir r))))
	    (ty0 (/ (- ymin (v-y (r-origin r))) (v-y (r-dir r))))
	    (ty1 (/ (- ymax (v-y (r-origin r))) (v-y (r-dir r))))
	    (tz0 (/ (- zmin (v-z (r-origin r))) (v-z (r-dir r))))
	    (tz1 (/ (- zmax (v-z (r-origin r))) (v-z (r-dir r)))))
	(let ((t0 (max tx0 ty0 tz0))
	      (t1 (min tx1 ty1 tz1)))
	  ;; Only return 
	  (if (< t0 t1)
	      ;; We hit!  Return only the non-negatives
	      (only-non-negatives t0 t1)
	      ;; Else
	      ;; No hit..
	      '()))))))

(defmethod ray-intx-t-vals ((prim box-primitive) ray)
  "Based on Revelles's octree traversal method"
  (let ((r (copy-ray ray)))
    ;; Adjust the ray's origin and direction
    (do-xyz ((dir-c (r-dir r))
	     (origin-c (r-origin r))
	     (mins-c (mins prim))
	     (maxs-c (maxs prim)))
      (if (= 0.0 dir-c)
	  (setf dir-c +epsilon+)
	  ;; Else
	  (if (< dir-c 0.0)
	      (progn (setf origin-c
			   (- (+ mins-c maxs-c)
			      origin-c))
		     (setf dir-c
			   (* -1 dir-c))))))
    (let ((xmin (v-x (mins prim))) (xmax (v-x (maxs prim)))
	  (ymin (v-y (mins prim))) (ymax (v-y (maxs prim)))
	  (zmin (v-z (mins prim))) (zmax (v-z (maxs prim))))
      (let ((tx0 (/ (- xmin (v-x (r-origin r))) (v-x (r-dir r))))
	    (tx1 (/ (- xmax (v-x (r-origin r))) (v-x (r-dir r))))
	    (ty0 (/ (- ymin (v-y (r-origin r))) (v-y (r-dir r))))
	    (ty1 (/ (- ymax (v-y (r-origin r))) (v-y (r-dir r))))
	    (tz0 (/ (- zmin (v-z (r-origin r))) (v-z (r-dir r))))
	    (tz1 (/ (- zmax (v-z (r-origin r))) (v-z (r-dir r)))))
	(let ((t0 (max tx0 ty0 tz0))
	      (t1 (min tx1 ty1 tz1)))
	  ;; Only return 
	  (if (< t0 t1)
	      ;; We hit!  Return only the non-negatives
	      (only-non-negatives t0 t1)
	      ;; Else
	      ;; No hit..
	      '()))))))


(defconstant +epsilon2+ 1e-4)
(defun approx-equal (a b)
  (and (< a (+ b +epsilon2+))
       (> a (- b +epsilon2+))))

(defmethod surface-normal ((prim box-primitive) pt)
  "Basically, this determines which of the six sides the point's on.
Because floating point isn't exact, we can't just check
that the point is EXACTLY on some side.  So, we'll just look at its
position relative to the box's center."
  ;; NOTE: This is a temporary implementation..not the most robust way to do this
  (let ((normal
	 (cond ((approx-equal (v-x pt) (v-x (mins prim))) #(-1 0 0))
	       ((approx-equal (v-x pt) (v-x (maxs prim))) #(1 0 0))
	       ((approx-equal (v-y pt) (v-y (mins prim))) #(0 -1 0))
	       ((approx-equal (v-y pt) (v-y (maxs prim))) #(0 1 0))
	       ((approx-equal (v-z pt) (v-z (mins prim))) #(0 0 -1))
	       ((approx-equal (v-z pt) (v-z (maxs prim))) #(0 0 1))
	       (t (error "Point not actually on box!")))))
    ;; Print out to trace output
    ;; (format *trace-output* "pt: ~a normal: ~a ~%" pt normal)
    normal))
    
'(defmethod print-object ((object box-primitive) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "(mins: ~a maxs: ~a)" (mins object) (maxs object))))

(define-formatting-print-method box-primitive box
  "mins: ~a -- maxs: ~a" (mins box) (maxs box))

