(in-package :an.steven.raytracer)

;; A ray being ray-traced
;; A raytracing ray.  Meaning, it goes in the opposite direction of the simulated photons.

(defclass-java-style
    shot ()
  (:public ray :ray nil)
  (:public trace-filter :trace-filter (make-instance 'no-filter))
  (:public adjusted-illum-fn :adjusted-illum-fn #'default-adjusted-illum-fn
	   :documentation "The ray was shot and some amount of light was calculated to reach the ray's origin.  Now, let the ray adjust this color with this function.  This is mainly used for things like modulating light from reflected/refracted rays.")
  (:public media :media '())
  )

(defmethod initialize-instance :after ((r shot) &key)
  ;; Let the media adjust the ray
  (dolist (med (media r))
    (apply-to-shot med r)
    (when (exclusive-medium? med) (return))))

;; We use functions here instead of generics for speed

(defun ray-ignores-model? (r model)
  (ignore-model? (trace-filter r) model))
(defun ray-ignores-hit? (r hit)
  (ignore-hit? (trace-filter r) hit))

;; TODO - think: Should we apply all the mediums..or just the first one?
(defun apply-shot-media-to-illum (r src-pt illum)
  "Go through the medium stack, applying them to the illum, until an exclusive medium is hit."
  (dolist (med (media r))
    (apply-to-illum med r src-pt illum)
    ;; (when (exclusive-medium? med) (return))
    )
  illum)

(defun shot-adjusted-illum (shot src-pt illum)
  ;; First, let the media adjust it (e.g. attenuate for fog)
  (let ((after-media (apply-shot-media-to-illum shot src-pt illum)))
    ;; Now let the adjustment function go
    (funcall (adjusted-illum-fn shot) after-media)))

(defun combined-light (hit ambient-light direct-illums secondary-illums)
  "This combines all the illumination and light that affect the hit, and returns the total resulting amount of light."
  (let ((out (shade-ambient-light (material hit) hit ambient-light)))
    ;; For each direct illum, shade it and accumulate
    (dolist (illum direct-illums)
      (when (non-nil? illum)
	(nv+ out
	     (shade-direct-illumination (material hit) hit illum))))
    ;; For each secondary, just add it
    (dolist (illum secondary-illums)
      (nv+ out (illum-light illum)))
    ;; Return the total
    out))

(defun get-hit-illum (hit scene depth-left)
  "We got a hit. Now figure out the color for it"
  ;; First, gather illuminations, direct and secondary
  (let ((direct-illums (get-direct-illums scene hit))
	(secondary-illums
	 ;; Only shoot secondary rays if we have enough depth left
	 (cond ((<= depth-left 1) '())
	       ;; Collect contributions from secondary rays
	       (t (loop for shot in (secondary-shots (material hit) hit)
			collect (nil-if-nil shot
					    ;; If not nil, shoot it
					    (get-shot-illum shot scene (1- depth-left))))))))
    ;; Now that we have our illuminations, combine them to get the total resulting light.
    ;; Also, let the ray itself adjust it before sending it back
    (shot-adjusted-illum
     (shot hit) (point hit)
     (make-illum
      :dir (vflip (r-dir (ray (shot hit))))
      :light (combined-light hit (ambient-light scene) direct-illums secondary-illums)))))
	 
(defun get-shot-illum (shot scene depth-left &key (use-bg-color nil))
  "This shoots the ray into the scene, gathers all the contributions, performs all adjustments, and returns the resulting illumination from the given ray."
  ;; (dbl depth-left)
  (let ((hit (get-ray-hit scene shot)))
    (cond ((non-nil? hit)
	   ;; Got a hit!  Get its resulting illumination.
	   (get-hit-illum hit scene depth-left))
	  ;; No hit - just return no illumination (or the bgcolor)
	  (t (make-illum :dir (make-vector 0 0 0)
			 :light (if use-bg-color
				    (background-color scene)
				    (make-vector 0 0 0)))))))

;; Common light adjustment functions

(defun make-modulate-by-scalar-fn (s)
  (lambda (illum)
    (make-illum
     :dir (illum-dir illum)
     :light (v* (illum-light illum) s))))

(defun make-modulate-by-color-fn (color)
  (lambda (illum)
    (make-illum
     :dir (illum-dir illum)
     :light (ncomp* (illum-light illum) color))))

;; Defaults
(defun default-adjusted-illum-fn (illum) illum)	; Just return the light unadjusted

(defun first-medium (r)
  (car (media r)))
(defun second-medium (r)
  (cadr (media r)))

(define-formatting-print-method shot s
  "ray: ~a media: ~a" (ray s) (media s))
