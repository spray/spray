(in-package :an.steven.raytracer)

;; Copied from http://www.angelcode.com/texgen/files/glass.as.txt
(defconstant +air-refraction-index+		1.00)
(defconstant +water-refraction-index+	1.33)
(defconstant +glass-refraction-index+	1.52)
(defconstant +quartz-refraction-index+	1.54)
(defconstant +diamond-refraction-index+	2.41)

;; Mediums can modify illuminations that transfer through them
;; Every medium has an index of refraction, which may or may not be used by the material

(defclass-java-style
  medium ()
  (:public index-of-refraction :index-of-refraction +air-refraction-index+))

(defmethod apply-to-shot ((medium medium) shot)
  "Given a raytracing ray that's going through the medium, this calculates the ray that results after it exits.  This function should destructively adjust the ray object!"
  ;; By default, do nothing to the ray
  nil)
(defmethod apply-to-illum ((medium medium) shot src-pt illum)
  "Once a ray has been shot and the illumination at the hit point has been found, the medium will have a chance to adjust that illumination before the ray adjusts it to get its final illumination."
  nil)
(defmethod exclusive-medium? ((medium medium))
  "If a medium is exclusive, then it overrides all other media it's in.  So if medium A is exclusive, and a ray entered medium B before medium A (so it's currently in both), then the ray's illumination would not be affected by B at all.  It would be adjusted as if the ray was only in A."
  nil)

;; Simple linear fog.  Mainly for testing media.
(defclass-java-style
  linear-fog (medium)
  (:readonly color :color (make-vector 0.69 0.69 0.69))	;; Default grey fog
  (:readonly vis-dist :vis-dist 100)	;; Beyond this distance, the fog completely envelopes everything.
  )
(defmethod apply-to-illum ((medium linear-fog) shot src-pt illum)
  (let ((dist (distance src-pt (r-origin (ray shot)))))
	(cond 
	  ((>= dist (vis-dist medium))
	   ;; It's beyond the visibility distance - set it to the fog color
	   (setf (illum-light illum) (color medium)))
	  (t
		;; Otherwise, figure out the ratios
		(let ((fog-ratio (/ dist (vis-dist medium))))
		  ;; Adjust the light color according to this ratio
		  (setf (illum-light illum)
				(nv+ (v* (color medium) fog-ratio)
					 (v* (illum-light illum) (- 1 fog-ratio)))))))))

;; Directional fog - kinda like how blinds or LCDs work
;; TODO

;; heat-warp - fog that randomly nudges the rays

;; Default medium classes

(defclass boring-medium (medium) ())
(defclass default-medium (boring-medium) ())

;; Instances

(defvar *glass-medium*
  (make-instance 'medium
		 :index-of-refraction +glass-refraction-index+))

(defvar *water-medium*
  (make-instance 'medium
		 :index-of-refraction +water-refraction-index+))
