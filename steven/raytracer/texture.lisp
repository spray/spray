(in-package :an.steven.raytracer)

(defclass-java-style texture-map-base ())
(defgeneric get-color (tmap uv))

;; Boring solids
(defclass-java-style
  solid-texture (texture-map-base)
  (:public color :color (make-vector 0 0 1)))
(defmethod get-color ((tmap solid-texture) uv)
  (color tmap))
(defun make-solid-map (color)
  (make-instance 'solid-texture :color color))

;; Checkerboard!
(defclass-java-style
  checkerboard-texture (texture-map-base)
  (:public color1 :color1 (make-vector 0 0 0))
  (:public color2 :color2 (make-vector 1 1 1))
  (:public num-reps :num-reps 2)	;; This should be at least 2 to be interesting
  )

(defun xor (a b)
  (not (eq a b)))

(defmethod get-color ((tmap checkerboard-texture) uv)
  (let* ((units-per-square (/ 1 (num-reps tmap)))
		 (ui (floor (/ (uv-u uv) units-per-square)))
		 (vi (floor (/ (uv-v uv) units-per-square))))
	(if (xor (evenp ui) (evenp vi))
	  ;; The evenness is different
	  (color1 tmap)
	  ;; Otherwise..
	  (color2 tmap))))

;; Linear gradient
;; TODO

;; Radial gradient
