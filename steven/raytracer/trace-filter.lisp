(in-package :an.steven.raytracer)

;; Trace filters.  These enforce ignoring of models and hits when tracing a shot ray.

(defclass-java-style
  trace-filter-base ())
(defgeneric ignore-model? (filter model))
(defgeneric ignore-hit? (filter hit))

;; First, a dummy
(defclass-java-style
  no-filter (trace-filter-base))
(defmethod ignore-model? ((filter no-filter) model) nil)
(defmethod ignore-hit? ((filter no-filter) hit) nil)

;; Specific kinds of filters

(defclass-java-style
  model-filter (trace-filter-base)
  (:public model :model nil))
(defmethod ignore-model? ((filter model-filter) model)
  (eql (model filter) model ))
(defmethod ignore-hit? ((filter model-filter) hit) nil)

;; hit filters - they never ignore models, only certain hits

(defclass-java-style hit-filter (trace-filter-base))
(defmethod ignore-model? ((filter hit-filter) model) nil)

(defclass-java-style
  inward-hit-filter (hit-filter)
  (:public model :model nil))
(defmethod ignore-hit? ((filter inward-hit-filter) hit)
  (when (eql (basic-model hit) (model filter))
	(when (inward-hit? hit)
	  t)))

(defclass-java-style
  outward-hit-filter (hit-filter)
  (:public model :model nil))
(defmethod ignore-hit? ((filter outward-hit-filter) hit)
  (when (eql (basic-model hit) (model filter))
	(when (outward-hit? hit)
	  t)))
