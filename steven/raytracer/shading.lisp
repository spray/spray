(in-package :an.steven.raytracer)

(defun at-least-0 (x)
  "If x < 0.0, returns 0.0.  Otherwise, returns x."
  (if (< x 0.0) 0.0 x))

;; Phong shading

(defun reflected-dir (to-reflect reflect-by)
  "Given a direction to the light and a surface normal, this returns the light vector reflected along the surface normal."
  (nv- (v* reflect-by
		   (* 2
			  (dot-product to-reflect reflect-by)))
	   to-reflect))

(defun lighting-eqn (diffuse specular light N.L V.R spec-exp)
  "I = I_l[ K_d(N.L) + K_s(V.R)^n ]"
  (* light
	 (+ (* diffuse N.L)
		(* specular
		   (zero-if-underflow (expt V.R spec-exp))))))

(defun phong-shade (
					to-light-dir light-color
					surface-norm to-viewer-dir
					diffuse specular spec-exp
					)
  "Note: This does NOT check if the surface is actually facing the light.  It's assumed it is."
  (let ((contrib (make-vector 0 0 0))
		(N.L (dot-product surface-norm to-light-dir))
		(refl-dir (reflected-dir to-light-dir surface-norm)))
	(let ((V.R (at-least-0 (dot-product to-viewer-dir refl-dir))))
	  (make-vector
		(lighting-eqn (v-x diffuse) (v-x specular) (v-x light-color) N.L V.R spec-exp)
		(lighting-eqn (v-y diffuse) (v-y specular) (v-y light-color) N.L V.R spec-exp)
		(lighting-eqn (v-z diffuse) (v-z specular) (v-z light-color) N.L V.R spec-exp)))))
