(in-package :an.steven.raytracer)

(defstruct uv u v)
(defclass-java-style uv-map ())
(defgeneric get-uv (uv-map pt))

(defun scale-uv! (x s)
  (setf (uv-u x)
	(* (uv-u x) s))
  (setf (uv-v x)
	(* (uv-v x) s))
  x)

(defun normalize-uv (uv)
  (make-uv
	:u (mod (uv-u uv) 1.0)
	:v (mod (uv-v uv) 1.0)))

;; Planar

(defclass-java-style
  xz-plane-uv-map (uv-map)
  (:public uv-scale :uv-scale 1.0))

(defmethod get-uv ((uv-map xz-plane-uv-map) pt)
  (make-uv :u (* (uv-scale uv-map) (v-x pt))
		   :v (* (uv-scale uv-map) (v-z pt))))

(defclass-java-style
  xy-plane-uv-map (uv-map)
  (:public uv-scale :uv-scale 1.0))

(defmethod get-uv ((uv-map xy-plane-uv-map) pt)
  (make-uv :u (* (uv-scale uv-map) (v-x pt))
		   :v (* (uv-scale uv-map) (v-y pt))))

;; Dummy UV maps

(defclass-java-style
  dummy-uv-map (uv-map))
(defmethod get-uv ((uv-map dummy-uv-map) pt)
  (make-uv :u 0 :v 0))

