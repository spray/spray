(in-package :an.steven.raytracer)

;; Models - this uses the composite pattern
;; The 'model' is the Component, the 'basic-model' is the Leaf, and the 'composite-model' is the Composite.
;; All hits will eventually record the basic (leaf) model that is hit.  The composites should all dispatch to the basics.

(defclass-java-style model ())
(defgeneric get-ray-hit (model shot))

;; The basic models are what's interesting.  They tell the ray what it hit, and how secondary rays behave.

(defclass-java-style
  basic-model (model)
  (:readonly material :material nil)
  (:readonly uv-map :uv-map (make-instance 'dummy-uv-map))	;; This maps points on the model onto points on the material
  (:readonly name :name "(no name)")
  )
(defgeneric get-trace-filter (model inward? outward?))
(defgeneric get-outward-trace-filter (model))
(defgeneric get-inward-trace-filter (model))
(defgeneric ray-intx-t-vals (model shot))
(defgeneric hit-for-t-val (model shot t-val))

'(defmethod get-trace-filter ((model basic-model) inward? outward?)
  (cond (inward?
	 (get-inward-trace-filter model))
	(outward?
	 (get-outward-trace-filter model))
	(t (error "we got a tangential hit..but that shouldn't happen"))))

(defmethod get-trace-filter ((model basic-model) inward? outward?)
  (cond (inward?
	 (get-inward-trace-filter model))
	(t
	 (get-outward-trace-filter model))))


(defun check-t-vals (t-vals)
  (let ((all-ok t)
	(prev-t-val (first t-vals)))
    (dolist (t-val t-vals)
      (when (or (< t-val 0.0)
		(< t-val prev-t-val))
	(setf all-ok nil)
	(return))
      (setf prev-t-val t-val))
    all-ok))

(defmethod get-ray-hit ((model basic-model) shot)
  "The main public interface for raytracing models.  This enforces trace-filters,
  and uses the generic ray-intx-t-vals to actually compute the intersections (if the model wasn't filtered out).
  ray-intx-t-vals will return an ASCENDING list of ray intersection t-vals, which this will then go through
  until it finds a non-filtered one."
  (let ((filter (trace-filter shot)))
    (cond ((ignore-model? filter model) nil)
	  (t
	   ;; This model isn't filtered out
	   ;; Get its t-vals
	   (let ((t-vals (assert-value (ray-intx-t-vals model shot)
				       t-vals
				       (check-t-vals t-vals)))
		 (first-passed-hit nil))
	     ;; Go through the hits, and see if they will get ignored
	     (dolist (t-val t-vals)
	       (let ((hit (hit-for-t-val model shot t-val)))
		 (cond ((not (ignore-hit? filter hit))
			;; It wasn't ignored!  Set it and return.
			(setf first-passed-hit hit)
			(return))
		       ;; Otherwise, keep going
		       (t nil))))
	     ;; Just return the first passed, even if it's nil
	     first-passed-hit)))))

;; The composite class

(defclass-java-style
  composite-model (model)
  (:public models :models '()))
(defmethod get-ray-hit ((model composite-model) shot)
  ;; Get the model that yields the closest hit
  ;; This uses a simple linear scan to find the best hit
  (let ((best-hit nil)
		(curr-hit nil))
	(dolist (m (models model))
	  (setf curr-hit (get-ray-hit m shot))
	  (when (hit-is-closer curr-hit best-hit)
		(setf best-hit curr-hit)))
	best-hit))

;; Specific categories of basic model

;; Flat model (polygons, infinite planes)

(defclass flat-model (basic-model) ())

(defmethod get-trace-filter ((model flat-model) inward? outward?)
  "For flat geometry, we always want to ignore the geometry itself
  for secondary rays.  If a ray is coming FROM the surface, it should
  not hit the surface again since it's flat."
  (make-instance 'model-filter :model model))
(defmethod get-outward-trace-filter ((model flat-model))
  (make-instance 'model-filter :model model))
(defmethod get-inward-trace-filter ((model flat-model))
  (make-instance 'model-filter :model model))

;; An infinite xy-plane
;; TODO - these planes should be primities..and we should just have a primitive-flat-model model class

(defclass-java-style
  xy-plane (flat-model)
  (:public dist :dist -100))

(defmethod ray-intx-t-vals ((model xy-plane) shot)
  (let ((s (r-origin (ray shot)))
	(d (r-dir (ray shot)))
	(dist (dist model)))
    (cond ((>= (v-z d) 0.0) '())	;; Ray is going outward, so it can't pierce this
	  (t
	   ;; Solve for t when z=0
	   ;; Basically, s.z + t*d.z = 0
	   ;; --> t = (5-s.z)/d.z
	   '(dbl (v-z s) (v-z d))
	   (let ((t-val (/ (- dist (v-z s))
			   (v-z d))))
	     (cond ((>= t-val 0.0)
		    ;; return it
		    (list t-val))
		   (t ;; No valid hit, return empty list
		    '())))))))
(defmethod hit-for-t-val ((model xy-plane) shot t-val)
  (make-instance 'hit
		 :t-val t-val
		 :shot shot
		 :normal (make-vector 0 0 1)
		 :material (material model)
		 :basic-model model))

(defclass-java-style
    xz-plane (flat-model)
  (:public dist :dist -100))

(defmethod ray-intx-t-vals ((model xz-plane) shot)
  (let ((s (r-origin (ray shot)))
	(d (r-dir (ray shot)))
	(dist (dist model)))
    (cond ((>= (v-y d) 0.0) '())	;; Ray is going outward, so it can't pierce this
	  (t
	   ;; Solve for t when y=0
	   ;; Basically, s.y + t*d.y = 0
	   ;; --> t = (5-s.y)/d.y
	   '(dbl (v-y s) (v-y d))
	   (let ((t-val (/ (- dist (v-y s))
			   (v-y d))))
	     (cond ((>= t-val 0.0)
		    ;; return it
		    (list t-val))
		   (t ;; No valid hit, return empty list
		    '())))))))
(defmethod hit-for-t-val ((model xz-plane) shot t-val)
  (make-instance 'hit
		 :t-val t-val
		 :shot shot
		 :normal (make-vector 0 1 0)
		 :material (material model)
		 :basic-model model))

;; Convex models

(defclass convex-model (basic-model) ())

;; Closed convex models, like circles

(defclass closed-convex-model (convex-model) ())

(defmethod get-outward-trace-filter ((model closed-convex-model))
  (make-instance 'model-filter :model model))
(defmethod get-inward-trace-filter ((model closed-convex-model))
  (make-instance 'inward-hit-filter :model model))

;; Single-primitive basics
;; This could be used for..spheres, ellipsoids
(defclass-java-style
  primitive-closed-convex-model (closed-convex-model)
  (:readonly primitive :primitive nil)
  )

(defmethod ray-intx-t-vals ((model primitive-closed-convex-model) shot)
  (ray-intx-t-vals (primitive model) (ray shot)))

(defmethod hit-for-t-val ((model primitive-closed-convex-model) shot t-val)
  (let ((hit-pt (point-on-ray (ray shot) t-val)))
    (make-instance 'hit
		   :t-val t-val
		   :shot shot
		   :normal (surface-normal (primitive model) hit-pt)
		   :material (material model)
		   :basic-model model)))

(defun make-sphere-model (center radius mat)
  (make-instance 'primitive-closed-convex-model
		 :primitive (make-instance 'sphere-primitive
					   :center center 
					   :radius radius)
		 :uv-map (make-instance 'spherical-uv-map
					:center center
					:uv-scale 8.0)
		 :material mat))

(defun make-box-model (mins maxs mat)
  (make-instance 'primitive-closed-convex-model
		 :primitive (make-instance 'box-primitive
					   :mins mins
					   :maxs maxs)
		 :material mat))

'(defmethod print-object ((object primitive-closed-convex-model) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "(primitive: ~a)" (primitive object))))

(define-formatting-print-method primitive-closed-convex-model obj
  "primitive: ~a" (primitive obj))

