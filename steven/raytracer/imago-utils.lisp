(in-package :an.steven.raytracer)

;; TODO - should we implement exponential exposure?

(defvar *exposure* 2)

(defun negative-light-p (light)
  (or (< (v-x light) 0.0)
	  (< (v-y light) 0.0)
	  (< (v-z light) 0.0)))

(defun light->brightness (l)
  "http://freespace.virgin.net/hugo.elias/graphics/x_posure.htm
  This converts an arbitrary (positive) light value to a [0,1] exposed brightness."
  '(when (< l 0.0) (dbl "light->brightness: negative light" l))
  (identity
  (- 1.0
	 (zero-if-underflow (exp (neg (* l *exposure*)))))))

(defun in-closed-range (x a b)
  (and (>= x a)
	   (<= x b)))

(defun crash-if-bad-rgb (c)
  (when (not (in-closed-range c #x00 #xFF))
	(error "bad rgb!!!"))
  c)

(defun light->color-component (l)
  "Maps [0,1] linearly to [0,255] U Integers.  Rounds with the 'round' function."
  (crash-if-bad-rgb (round (* (light->brightness l) 255))))

(defun rgb->vector-comp (c)
  (/ c 255.0))
(defun rgb->vector (rgb)
  (make-vector
	(rgb->vector-comp (color-red rgb))
	(rgb->vector-comp (color-green rgb))
	(rgb->vector-comp (color-blue rgb))))

;; TODO rename to light->color-vector
(defun vec->color (v)
  "Returns the 32-bit imago color based on vector v.  v is assumed to be normalized, meaning each component is in [0,1].  This map is effectively [0,1]->[0,256].  The 'round' function is used to round.  And (x,y,z)->(r,g,b)."
  (make-color (light->color-component (v-x v))
			  (light->color-component (v-y v))
			  (light->color-component (v-z v))))

(defun make-image (width height)
  (make-instance 'rgb-image
				 :width width
				 :height height))

