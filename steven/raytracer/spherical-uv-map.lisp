(in-package :an.steven.raytracer)

;; Planar

(defclass-java-style
    spherical-uv-map (uv-map)
  (:public center :center (error "No center provided to spherical UV map!"))
  (:public uv-scale :uv-scale 1.0))

(defun xy-projection (v)
  (vector (v-x v) (v-y v) 0))
(defun xz-projection (v)
  (vector (v-x v) 0 (v-z v)))

(defconstant 2-pi (* 2 pi))
(defconstant spherical-uv-origin #(1 0 0))

(defun spherical-uv (center pt uv-scale)
  (let ((r (normalize-f (v- pt center))))
    (let ((v (/ (acos (v-z r)) pi))
	  (x (v-x r))
	  (y (v-y r)))
;;      (dbl v)
      (scale-uv!
       (make-uv :v v
		:u (identity
		    (+ (/ (acos (clamp-to (/ x (sin (* pi
						       (if (= 0.0 v) 1e-9
							   v))))
					  -1.0 1.0))
			  (+ pi pi))
		       (if (>= (v-y r) 0.0) 0.0
			   pi))))
       uv-scale))))

;;(step (SPHERICAL-UV #(3 -3 -5) #(0.10344825 -2.9999995 -2.2413778) 1.0))

(defmethod get-uv ((uv-map spherical-uv-map) pt)
  (spherical-uv (center uv-map) pt (uv-scale uv-map)))
