(in-package :an.steven.raytracer)

;; A square imago-image texture

(defclass-java-style
  image-texture (texture-map-base)
  (:readonly img :img nil))

(defun uv-to-image-xy (u v width height)
  "Returns a pair of XY pixel coordinates"
  (values (round (* u width))
		  (round (* v height))))

(defun img-color-at-uv (u v img)
  (multiple-value-bind (x y) (uv-to-image-xy
							   u v
							   (1- (image-width img))
							   (1- (image-height img)))
	(rgb->vector (image-pixel img x y))))

(defmethod get-color ((tmap image-texture) uv)
  (let ((uv (normalize-uv uv)))
	(img-color-at-uv (uv-u uv) (uv-v uv)
					 (img tmap))))
