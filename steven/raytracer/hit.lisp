(in-package :an.steven.raytracer)

;; A raytracing hit
(defclass-java-style
  hit ()
  (:public t-val :t-val nil)
  (:public shot :shot nil)
  (:public normal :normal nil)
  (:public material :material nil)
  (:public basic-model :basic-model nil)

  (:public point :point nil)
  )
(defmethod initialize-instance :after ((h hit) &key)
  (setf (point h) (point-on-ray (ray (shot h)) (t-val h))))

;; Public interface

(defmethod ray-norm-dotp ((h hit))
  (dot-product (r-dir (ray (shot h))) (normal h)))

(defun hit-is-closer (hit1 hit2)
  (cond ((nil? hit2) t)
		((nil? hit1) nil)
		(t (< (t-val hit1) (t-val hit2)))))

;; Utility functions

(defun outward-hit? (hit)
  "This returns true if the ray seems to be going out of the hit surface.  It simply compares the direction of the hit to the surface normal."
  (> (ray-norm-dotp hit) 0.0))

(defun inward-hit? (hit)
  "Similar to above"
  (< (ray-norm-dotp hit) 0.0))

(defun tangential-hit? (hit)
  "Similar to above"
  (= (ray-norm-dotp hit) 0.0))

'(defmethod print-object ((object hit) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "(shot: ~a normal: ~a basic-model: ~a point: ~a)"
	    (shot object) (normal object) (basic-model object) (point object))))

(define-formatting-print-method hit h
  "shot: ~a
normal: ~a
basic-model: ~a
point: ~a"
  (shot h) (normal h) (basic-model h) (point h))
