(in-package :an.steven.raytracer)

;; Illumination is like a light ray, but it represents just the energy that hits some point.
;; So it's some energy that hits at some direction.

(defstruct illum
  dir
  light)
