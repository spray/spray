(in-package :an.steven.raytracer)

;; Views

(defclass raytracing-view-base () ())
(defgeneric view-origin (view))
(defgeneric get-viewplane-point (view u v))
(defgeneric get-viewplane-ray (view u v))

(defmethod get-viewplane-ray ((view raytracing-view-base) u v)
  "This returns a ray from the view origin that goes through the given viewplane point."
  (make-ray-from-line
	(view-origin view)
	(get-viewplane-point view u v)))

;; A very simple, static view.  Mainly for debugging.
(defclass simple-RT-view (raytracing-view-base) ())
(defmethod view-origin ((view simple-RT-view))
  (make-vector 0 0 5))	; Looking at the origin
(defmethod get-viewplane-point ((view simple-RT-view) u v)
  "Simulate a 10x10 viewing plane, so [-1,1]->[-5,5]"
  (make-vector (* u 5) (* v 5) -2))

;; Perspective view - TODO low priority
(defclass perspective-view (raytracing-view-base)
  (origin dir fov bl-corner tl-corner br-corner))
(defmethod view-origin ((view perspective-view))
  (slot-value view 'origin))
(defmethod get-viewplane-point ((view perspective-view) u v)
  (list u v)
  ;; TODO
  )
'(defmethod initialize-instance ((view perspective-view))
   ;; TODO - calculate the corner points
   )


