(in-package :an.steven.raytracer)

;; Materials
(defclass material-base () ())
;; Public interface
(defgeneric secondary-shots (material hit)
  (:documentation "When a light ray hits an object, it might get scattered in many directions as secondary outgoing rays, depending on what kinda material it is.  This function returns a list of the outgoing rays given the hit point and the incoming direction.  These usually are the reflection/refraction rays, but could be more (or none)."))

(defgeneric shade-direct-illumination (material hit illum)
  (:documentation "Given the direct illumination that reaches the hit, this calculates the resulting contribution it makes to the hit's final color.  Usually, it will just be a phong shading function."))

(defgeneric shade-ambient-light (material hit light))


;; Simple Phong materials
(defclass-java-style
    phong-material (material-base)
  (:public diffuse-map :diffuse-map (make-instance 'solid-texture))
  (:public specular-color :specular-color (make-vector 1 1 1))
  (:public specular-exponent :specular-exponent 5)
  (:public reflectivity :reflectivity 1)	;; HACK!
  )

(defmethod diffuse-color ((mat phong-material) hit)
  (get-color (diffuse-map mat)
	     (get-uv (uv-map (basic-model hit))
		     (point hit))))

(defun get-path-type (primary-inward? secondary-inward?)
  (if primary-inward?
      (if secondary-inward?
		  :entering-path
		  :external-bouncing-path)
      (if secondary-inward?
		  :internal-bouncing-path
		  :exiting-path)))

'(deftest test-get-path-type ()
  (check (eq (get-path-type nil nil) :exiting-path)
   (eq (get-path-type nil t) :internal-bouncing-path)
   (eq (get-path-type t nil) :external-bouncing-path)
   (eq (get-path-type t t) :entering-path)))

(defun compute-path-type (primary-hit secondary-ray)
  (let ((primary-inward? (inward-hit? primary-hit))
		(secondary-inward? (inward-dir? (r-dir secondary-ray) (normal primary-hit))))
    (get-path-type primary-inward? secondary-inward?)))

(defmethod media-for-secondary-shot ((material phong-material) primary-shot path-type)
  "Rays can only bounce off phong materials, so assume it's :external-bouncing-path.
  Just return the media of the primary shot"
  (media primary-shot))

;; Reflection

(defun reflected-ray (outward-inc-dir hit-pt surface-normal)
  "Returns the ray that starts from the hit point and goes in the direction that the incoming ray would reflect off at."
  (make-ray hit-pt (reflected-dir outward-inc-dir surface-normal)))

(defun reflected-shot (ray filter fresnel-term media)
  (make-instance
   'shot
   :ray ray
   :trace-filter filter
   :adjusted-illum-fn (make-modulate-by-scalar-fn fresnel-term)
   :media media
   ))

(defmethod get-secondary-shot ((material phong-material) hit to-pt)
  "Returns a smart shot from the hit to the to-pt"
  (let ((ray (make-ray-from-line (point hit) to-pt))
	(hit-model (basic-model hit))
	(surface-normal (normal hit)))
    (let ((outward-ray-p (outward-dir? (r-dir ray) surface-normal))
	  (inward-ray-p (inward-dir? (r-dir ray) surface-normal)))
      (make-instance
       'shot
       :ray ray
       :trace-filter (get-trace-filter hit-model inward-ray-p outward-ray-p)
       :media (media-for-secondary-shot material
					(shot hit)
					(compute-path-type hit ray))))))

(defmethod secondary-shots ((mat phong-material) hit)
  (if (= 0.0 (reflectivity mat)) '()
      ;; Otherwise, return the reflected shot
      (let* ((outward-inc-dir (vflip (r-dir (ray (shot hit))))))
	(list (make-instance
	       'shot
	       :ray (reflected-ray outward-inc-dir (point hit) (normal hit))
	       :trace-filter (get-outward-trace-filter (basic-model hit))
	       :adjusted-illum-fn (make-modulate-by-color-fn (v* (diffuse-color mat hit) (reflectivity mat)))
	       :media (media (shot hit)))))))

(defmethod phong-shade-illum ((mat phong-material) illum surface-norm shot hit)
  (let ((light (phong-shade (v* (illum-dir illum) -1)	; Direction to the light source
			    (illum-light illum)
			    surface-norm
			    (v* (r-dir (ray shot)) -1)	; to-viewer-dir.  The viewer being, the incoming ray
			    (diffuse-color mat hit)
			    (specular-color mat)
			    (specular-exponent mat))
	  ))
    '(when (negative-light-p light)
      (dbl "negative light from phong-shading" light))
    light))

(defmethod shade-direct-illumination ((m phong-material) hit illum)
  (phong-shade-illum m illum (normal hit) (shot hit) hit))

(defmethod shade-ambient-light ((m phong-material) hit light)
  (comp* light (diffuse-color m hit)))

