(in-package :an.steven.raytracer)

;; Fresnel materials

;; Refraction

(defun fresnel-term (a-in a-out n-coeff)
  "a-in - incoming angle to normal
  a-out - outgoing angle to normal
  This represents the fraction of light that refracts."
  (^2 (cond ((/= 0 (+ a-in a-out))
			 (/ (sin (- a-in a-out))
				(sin (+ a-in a-out))))
			(t (/ (- n-coeff 1)
				  (+ n-coeff 1))))))

(defun refracted-angle-by-snells-law (a-in n-coeff)
										;(dbl "refracted angle" a-in n-coeff)
  (let ((x (* (sin a-in) n-coeff)))
	(if (> x 1.0)
		;; A-in is past the critical angle
		;; Total internal reflection!
		nil
		;; A-in is OK, calculate A-out
		(asin x))))

(defun refracted-angle (a-in n-coeff)
  (refracted-angle-by-snells-law a-in n-coeff))

(defun refracted-dir (inc-dir n-coeff normal)
  "From http://www.cs.unc.edu/~rademach/xroads-RT/RTarticle.html"
  (let* ((c1 (dot-product inc-dir normal))
		 (c2 (sqrt (- 1 
					  (* (^2 n-coeff)
						 (- 1 (^2 c1)))))))
	(if (complexp c2) nil	;; Total internal reflection
		;; Otherwise, calculate the refracted dir
		(normalize-f
		 (nv+ (v* inc-dir n-coeff)
			  (v* normal
				  (- (* n-coeff c1)
					 c2)))))))

(defun refracted-ray (inc-dir hit-pt n-coeff surf-norm)
  "Assumes in-ray is going INTO the surface"
  (let ((trans-dir (refracted-dir inc-dir n-coeff surf-norm)))
	(nil-if-nil trans-dir
				(make-ray hit-pt trans-dir))))

;; Fresnel material class

;; TODO - need to do some refactoring.  Like, with this model, we can't even do volumetric fog!  All transparent objects now are forced to be fresnel materials.
;; The medium must be a fresnel-medium, with an appropriate index-of-refraction method
(defclass-java-style
	fresnel-material (phong-material)
  (:public inner-medium :inner-medium nil))

(defmethod media-for-secondary-shot ((material fresnel-material) primary-shot path-type)
  "For fresnel materials, secondary shots can be more than just surface bounces"
  ;; TODO - we should just use case here..
  (switch path-type
	  (
	   ;; Exiting - pop off the top medium
	   (:exiting-path (cdr (media primary-shot)))
	   ;; Entering - push the inner medium onto the media stack
	   (:entering-path (cons (inner-medium material) (media primary-shot)))
	   ;; Bouncing - leave it as is
	   (:external-bouncing-path (media primary-shot))
	   (:internal-bouncing-path (media primary-shot))
	   )
	  ;; Default - error!
	  (error "Bad path-type given!")))

(defmethod exiting-secondary-shots ((material fresnel-material) hit)
  "This returns a refracted outward shot, or just an internally reflected shot"
  (let* ((inward-normal (vflip (normal hit)))
	 (inward-incident-direction (vflip (r-dir (ray (shot hit)))))
	 (n-coefficient (/ (index-of-refraction (inner-medium material))
			   (index-of-refraction (second-medium (shot hit)))))
	 (reflected-ray (reflected-ray inward-incident-direction
				       (point hit) inward-normal))
	 (a-in (angle-between-normals inward-incident-direction
				      inward-normal))
	 (a-out (refracted-angle a-in n-coefficient)))
    (if a-out
	;; There is an outward refracted angle.
	;; Just return that
	(list
	 (make-instance
	  'shot
	  :ray (refracted-ray (r-dir (ray (shot hit)))
			      (point hit)
			      n-coefficient
			      inward-normal)
	  ;; This refraction is going outward
	  :trace-filter (get-outward-trace-filter (basic-model hit))
	  ;; Don't do anything to the illumination
	  :adjusted-illum-fn #'identity
	  ;; Pop the top medium off
	  :media (cdr (media (shot hit)))))
	
	;; Else..
	;; There is just a reflected shot
	;; Total internal reflection
	(list
	 (make-instance
	  'shot
	  :ray (reflected-ray inward-incident-direction
			      (point hit)
			      inward-normal)
	  ;; The reflection is inward
	  :trace-filter (get-inward-trace-filter (basic-model hit))
	  ;; Don't do anything to the illumination
	  :adjusted-illum-fn (make-modulate-by-color-fn (specular-color material))
	  ;; Keep the medium
	  :media (media (shot hit)))))))

(defmethod entering-secondary-shots ((material fresnel-material) hit)
  ;; Calculate some necessary values
  (let* ((outward-incident-direction (vflip (r-dir (ray (shot hit)))))
	 (n-coefficient (/ (index-of-refraction (first-medium (shot hit)))
			   (index-of-refraction (inner-medium material))))
	 (reflected-ray (reflected-ray outward-incident-direction
				       (point hit) (normal hit)))
	 (a-in (angle-between-normals outward-incident-direction
				      (normal hit)))
	 (a-out (refracted-angle a-in n-coefficient)))
    
    (if a-out
	
	;; There is a refracted angle.  Calculate the fresnel term and return both shots
	(let ((fresnel-term (fresnel-term a-in a-out n-coefficient)))
	  (list
	   ;; The reflected
	   (make-instance
	    'shot
	    :ray reflected-ray
	    :trace-filter (get-outward-trace-filter (basic-model hit))
	    :adjusted-illum-fn (make-modulate-by-color-fn (v* (specular-color material) fresnel-term))
	    :media (media (shot hit)))
	   ;; The refracted/transmitted
	   (make-instance
	    'shot
	    :ray (refracted-ray (r-dir (ray (shot hit)))
				(point hit)
				n-coefficient
				(normal hit))
	    :trace-filter (get-inward-trace-filter (basic-model hit))
	    :adjusted-illum-fn (make-modulate-by-scalar-fn (- 1 fresnel-term))
	    ;; Push the inner media onto the stack
	    :media (cons (inner-medium material) (media (shot hit))))))
	
	;; Else..
	;; There is just a reflected shot
	(list
	 (make-instance
	  'shot
	  :ray reflected-ray
	  :trace-filter (get-outward-trace-filter (basic-model hit))
	  ;; :adjusted-illum-fn (make-modulate-by-scalar-fn (reflectivity material))
	  :adjusted-illum-fn (make-modulate-by-color-fn (specular-color material))
	  :media (media (shot hit)))))))

(defmethod secondary-shots ((material fresnel-material) hit)
  ;; Is it coming in, or going out?
  (cond ((inward-hit? hit)
	 (entering-secondary-shots material hit))
	(t
	 (exiting-secondary-shots material hit))))

;; Common Fresnel materials

(defvar *glass-material*
  (make-instance 'fresnel-material
		 :inner-medium *glass-medium*
		 :diffuse-map (make-solid-map (make-vector 0.1 0.1 0.1))
		 :specular-color (make-vector 1 1 1)	;; White
		 :specular-exponent 25	;; Pretty sharp shinyness
		 ))

(defvar *water-material*
  (make-instance 'fresnel-material
		 :inner-medium *water-medium*
		 :diffuse-map (make-instance
			       'image-texture
			       :img (read-png "water2.png"))
		 :specular-color (make-vector 0.5 0.5 1.0)
		 :specular-exponent 2))
