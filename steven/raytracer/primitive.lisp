(in-package :an.steven.raytracer)

;; Geometric primitives, like spheres, triangles, AABBs, etc.

(defclass primitive-base () ())
(defgeneric ray-intx-t-vals (prim ray))
(defgeneric surface-normal (prim pt))

;; Spheres

(defclass-java-style
  sphere-primitive (primitive-base)
  (:public center :center (make-vector 0 0 0))
  (:public radius :radius 1.0)
  )

(defmethod ray-intx-t-vals ((prim sphere-primitive) ray)
  (ray-sphere-intersection-t-vals ray (center prim) (radius prim)))
(defmethod surface-normal ((prim sphere-primitive) pt)
  (sphere-normal-at (center prim) (radius prim) pt))
(defun make-sphere (center radius)
  (make-instance 'sphere-primitive :center center :radius radius))

'(defmethod print-object ((prim sphere-primitive) stream)
	(dbl (center prim) (radius prim)))

(define-formatting-print-method
    sphere-primitive sphere
  "center: ~a radius: ~a" (center sphere) (radius sphere))
