;; 27-07-2005 23:59:07
;; This isn't up to date.  We need to fit this into the illumination model

(in-package :an.steven.raytracer)

;; Lights - primary base class

"Lights emit light energy in the form of beams.  Given a scene, an object, and a point on the object, a light will determine how much (if any) of its energy hits that point and from what direction."
(defclass light-base () ())
(defgeneric illumination-from (light hit scene)
  (:documentation "This returns the illumination that this light contributes to the given hit, taking into account the given scene"))

;; Shadowing lights

(defclass shadowing-light (light-base) ())
(defgeneric unblocked-illum-color (light hit) (:documentation "Assuming the path from the light to the hit point is unblocked, how much illumination should this light contribute?"))
(defgeneric dir-to-pt (light pt) (:documentation "This should return the direction from the light to the given pt"))

(defun get-shadow-ray (light-pt hit)
  ;; Shadow rays are always bounced
  (get-secondary-shot (material hit) hit light-pt))

(defun shadow-ray-blocked (s-ray hit scene hit-to-light-dist)
  "Shoots a ray from surface to light and see if it hits anything in between."
  (let ((rayhit (get-ray-hit
		 scene
		 s-ray))
	(hit-pt (point hit)))	
    (cond (rayhit ;; There was a hit.  Hmm..was it closer than the light?
	   (cond ((< (t-val rayhit) hit-to-light-dist)
		  ;; The hit was closer than the light!
		  ;; Return true - it was blocked
		  t)
		 ;; Otherwise..we're OK.  Return false.
		 (t nil)))
	  ;; No hit - nothing is blocking
	  (t nil))))

(defmethod illumination-from ((light shadowing-light) hit scene)
  "If the light can see the hit pt (ie. the shadow ray isn't blocked), this return how much illumination the light contributes to the point."
  ;; Are we even facing the surface?
  (let ((light-to-point-dir (dir-to-pt light (point hit))))
    (if (> (dot-product light-to-point-dir (normal hit)) 0.0)
	;; The light isn't even facing the hit point
	nil
	;; Otherwise..calculate the shadow ray and see if it's blocked
	(let ((s-ray (get-shadow-ray (origin light) hit)))
	  (if (not (shadow-ray-blocked s-ray hit scene (distance (point hit) (origin light))))
	      ;; It's not blocked
	      ;; Get the unblocked light and direction and return an illumination from them.
	      ;; Also, let the shadow-ray adjust the illumination
	      (progn
		(shot-adjusted-illum
		 s-ray
		 (origin light)
		 (make-illum
		  :dir light-to-point-dir
		  :light (unblocked-illum-color light hit)
		  )))
	      ;; Otherwise, return nil
	      nil)))))

;; Point-lights

(defclass point-light-base (shadowing-light)
  ((origin :initarg :origin
	   :reader origin)
   (color :initarg :color
	  :reader color)
   ))
(defmethod dir-to-pt ((light point-light-base) pt)
  (dir-from (origin light) pt))

;; The default linear-fall off point light

(defclass-java-style
    point-light (point-light-base)
  (:public fall-off :fall-off 100))

(defun linearly-attenuated-energy (light dist)
  (let ((att-factor
	 ;; Calculate the linear 1/r factor
	 (cond ((< dist (fall-off light)) 1.0)
	       (t (/ (fall-off light) dist)))))
    ;; Just return the light's energy scaled by the attenuation factor
    (v* (color light) att-factor)))

(defmethod unblocked-illum-color ((light point-light) hit)
  (linearly-attenuated-energy light (distance (point hit) (origin light))))

