(in-package :an.steven.raytracer)

(defun format-vector (stream v)
  (format stream "~a ~a ~a" (v-x v) (v-y v) (v-z v)))

;; Scene monkey is a program that just displays spheres and rays and lights and stuff

(defgeneric print-to-scene-monkey-file (stream obj))


(defmethod print-to-scene-monkey-file (stream obj)
  "The default printer..doesn't do much"
  (format stream "Unknown object type - could not print~%"))

;; Geometric primitives

(defmethod print-to-scene-monkey-file (stream (obj sphere-primitive))
  (format stream "sphere ")
  (format-vector stream (center obj))
  (format stream " ~a~%" (radius obj)))

;;
(defmethod print-to-scene-monkey-file (stream (obj shot))
  "This just prints the shot's ray to file"
  (format stream "ray ")
  (format-vector stream (r-origin (ray obj)))
  (format stream " ")
  (format-vector stream (r-dir (ray obj)))
  (format stream "~%"))

(defmethod print-to-scene-monkey-file (stream (hit hit))
  "Print the hit as a line, from the ray's origin to the hit point"
  (format stream "line ")
  (format-vector stream (r-origin (ray (shot hit))))
  (format stream " ")
  (format-vector stream (point hit))
  (format stream "~%"))

(defmethod print-to-scene-monkey-file (stream (obj point-light))
  (format stream "light ")
  (format-vector stream (origin obj))
  (format stream "~%"))

;; Models


(defmethod print-to-scene-monkey-file (file (obj primitive-closed-convex-model))
  (print-to-scene-monkey-file file (primitive obj)))

(defmethod print-to-scene-monkey-file (file (obj composite-model))
  (dolist (model (models obj))
    (print-to-scene-monkey-file file model)))

(defmethod print-to-scene-monkey-file (file (obj scene))
  (print-to-scene-monkey-file file (model obj))
  (dolist (light (lights obj))
    (print-to-scene-monkey-file file light)))

;; The convenient global interface

(defvar *scene-monkey-output* nil)

(defun print-to-scene-monkey (obj)
  (when *scene-monkey-output*
    (with-open-file (file "scene-monkey-output.txt"
			  :direction :output
			  :if-exists :append
			  :if-does-not-exist :create)
      (print-to-scene-monkey-file file obj))))
