(in-package :an.steven.raytracer)

(defvar *depth-limit* 4)

;; This mainly deals with the interface between the raytraced 2D image and everything else.

(defun calc-UV (width height x y)
  "This returns the coordinates of (x,y) in the following coordinate system defined for the given the screen (width/height): The middle of the screen is 0,0 and 1,1 is the top-right corner, and -1,-1 is the bottom left corner."
  (let ((half-x (/ width 2))
		(half-y (/ height 2)))
	(let ((trans-x (- x half-x))
		  (trans-y (- y half-y)))
	  ;; UV coords are normalized, so divide the translated coords by the "radius" of the screen
	  (values (/ trans-x half-x)
			  (/ trans-y half-y)))))

(defun calc-viewplane-ray (width height x y view)
  "You're using a screen of size w/h to render the given view.
  Then, this returns the viewplane ray that goes through pixel (x,y) on the screen."
  (multiple-value-bind (u v) (calc-UV width height x y)
	(get-viewplane-ray view u v)))

(defun raytrace-screen-point (x y w h scene view)
  "Given a point (x,y) on the screen (w,h), this raytraces for it and gets the resulting color."
  '(dbl "raytrace-screen-point" x y)
  (let ((ray (calc-viewplane-ray w h x y view)))
	;; Return the light gathered by the shot ray
	(illum-light (get-shot-illum
		      ;; Create the ray-shot object
		      (make-instance 'shot
				     :ray ray
				     :media (list (empty-space-medium scene)))
		      scene
		      *depth-limit*
		      :use-bg-color t))))

(defvar *super-sampling-level* 0)
(defun raytrace-pixel (x y w h scene view &key (super-sampling-level *super-sampling-level*))
  (if (= 0 super-sampling-level)
	;; No super sampling.  Just do it directly.
	(raytrace-screen-point x y w h scene view)
	;; Super sample it.
	(raytrace-pixel-super-sampled x y w h scene view super-sampling-level)))

(defun img-y->world-y (y height)
  (1- (- height y)))
(defun world-y->img-y (y height)
  (1- (- height y)))

(defun trace-img-pixel (x y) nil)	;; For debugging

(defun raytrace-img (img view scene)
  "The main raytracing loop for a 2D imago-image"
  (let ((w (image-width img))
		(h (image-height img))
		y
		light
		(beater (make-instance 'heartbeater :period-secs 1)))
	(dbl "raytracing image" w h)
	;; Ray tracing loop
	(do-image-pixels (img pix x flipped-y)
					 '(dbl "---" x flipped-y)
					 (trace-img-pixel x flipped-y)
					 (try-beat beater #'(lambda () (dbl "at pixel" x y)))
					 (setf y (1- (- h flipped-y)))

					 ;; Raytrace it!
					 (setf light (raytrace-pixel x y w h scene view))

					 ;; Check it
					 '(when (negative-light-p light)
						(dbl "negative light!!" light x flipped-y))

					 ;; Set the color in the image
					 (setf pix (vec->color light))
					 ; (setf pix (vec->color #(1 0 1))) 	;; DEBUG

					 (let ((c (vec->color light)))
					   (crash-if-bad-rgb (color-red c))
					   (crash-if-bad-rgb (color-green c))
					   (crash-if-bad-rgb (color-blue c)))
					  
					 )))

(defun raytrace-scene-to-png (scene png-file width height view &key (skip-writing nil))
  "Just for conveniance."
  (let ((img (make-image width height)))
	(time (raytrace-img img view scene))
	(unless skip-writing
	  (dbl "writing " png-file "..")
	  (write-png img png-file))))

(defun raytrace-scene-to-tga (scene filename width height view &key (skip-writing nil))
  "Just for conveniance."
  (let ((img (make-image width height)))
	(time (raytrace-img img view scene))
	(unless skip-writing
	  (dbl "writing " filename "..")
	  (write-tga img filename))))

