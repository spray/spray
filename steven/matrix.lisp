(in-package :an.steven.matrix)

;; Create a m-rows by n-cols array
(defmacro make-matrix (m n initial-element)
  `(make-array (list ,m ,n) :initial-element ,initial-element))

;;; Accessors

(defmacro num-rows (M) `(array-dimension ,M 0))
(defmacro num-cols (M) `(array-dimension ,M 1))
(defmacro last-row (M) `(1- (num-rows ,M)))
(defmacro last-col (M) `(1- (num-cols ,M)))
(defmacro matrix-elt (M i j) `(aref ,M ,i ,j) )
(defaliases matrix-elt mtx-elt mat-elt)

(defmacro same-dims (A B)
  "Returns true if A and B have the same dimensions"
  `(and (= (num-rows ,A) (num-rows ,B))
		(= (num-cols ,A) (num-cols ,B))))

;;; Iterators

(defmacro domatrix-by-index ((i j M) &rest do-form)
  "Iterates across M, binding just the indices to i,j."
  `(loop for ,i from 0 to (last-row ,M) do
		 (loop for ,j from 0 to (last-col ,M) do
			   ,@do-form )))

(defmacro domatrix-by-elt ((e M) &rest do-form)
  "Iterates across M, binding just the element to e."
  `(domatrix-by-index (i j ,M)
					  (let ((,e (mtx-elt ,M i j)))
						,@do-form)))

(defmacro domatrix ((i j e M) &rest do-form)
  "Iterates across M, binding indices to i,j and the element to e."
  `(domatrix-by-index (,i ,j ,M)
					  (let ((,e (mtx-elt ,M ,i ,j)))
						,@do-form)))

(defmacro domatrix-rows ((i M) &rest do-form)
  "Iterates across the rows of M, binding the row index to i."
  `(loop for ,i from 0 to (last-row ,M) do
		 ,@do-form))

(defmacro domatrix-cols ((j M) &rest do-form)
  "Ditto, for columns."
  `(loop for ,j from 0 to (last-col ,M) do
		 ,@do-form))

;;; Operations

(defmacro make-matrix-same-size (src initial-element)
  `(make-matrix (num-rows ,src) (num-cols ,src) ,initial-element))

(defmacro mat-copy (src dest)
  "Copies dest's elements to src"
  `(progn
	 (unless (same-dims ,src ,dest)
	   (dbl "Tried to copy a matrix into another matrix, but they don't have the same sizes")
	   nil)
	 (domatrix (i j e ,src)
			   (setf (mat-elt ,dest i j) e))))

;; TODO - make this a macro?
(defun make-mat-copy (src)
  "Returns a copy of src"
  (let ( (P (make-matrix-same-size src 0)) )
	(mat-copy src P)
	P))

(defun naive-mult (A B)
  "Straight forward matrix multiplication.  A simple minded O(n^3) algorithm."
  ; TODO: check matrix dimensions and return error
  (let ((max-k (last-col A))
		(P (make-matrix (num-rows A) (num-cols B) 0)))
	(domatrix-rows (i A)
    (domatrix-cols (j B)
	  (loop for k from 0 to max-k do
			`((dbl "---------")
			  (dbl i j k)
			  (dbl (mtx-elt A i k))
			  (dbl (mtx-elt B k j))
			  (dbl (mtx-elt P i j))
			  (show-matrix P))
			(incf (mtx-elt P i j)
				  (* (mtx-elt A i k)
					 (mtx-elt B k j) )))))
	P))
(defaliases naive-mult mat* mtx*)

(defun add-2-matrices (A B)
  "Returns the result of A+B"
  (unless (same-dims A B)
	(dbl "Tried to add two matrices of different dimensions")
	nil)
  ;; Add 'em element-wise
  (let ((S (make-mat-copy A)))	; Start with a copy of A
	;; Then add every elt of B to the corresponding elt in S
	(domatrix (i j e B)
	  (incf (mat-elt S i j) e))
	S))

(defmacro mat+ (&rest matrices)
  "Adds all given matrices together."
  ;; Add the first argument to something..
  `(add-2-matrices
	 ,(first matrices)
	 ;; Now decide what to add it to..
	 ,(if (= 2 (length matrices))
		;; The base case
		;; Add the first to the second.  We're done
		(second matrices)
		;; The recurisve case
		;; Add the first to the result of adding the rest
		`(mat+ ,@(rest matrices)) )))

(defun show-matrix (M)
  "Displays the matrix using (format t ..)"
  (domatrix-rows (i M)
	(format t "| ")
	(domatrix-cols (j M)
				   (format t "~a " (mtx-elt M i j)))
	(format t "|~%")))

(defun mtx-equal (A B)
  "Performs element-wise /='s to look for mismatches"
  ;; First check dimensions
  (if (not (same-dims A B))
	;; Dimensions don't match.. !=
	nil
	;; Dimensions are OK
	;; Now check elements
	(block element-checking
		   ;; Check each element of both matrices for equality
		   (domatrix (i j e A)
					 (if (/= e (mtx-elt B i j))
					   ;; We found a mismatch
					   (progn
						 ;; Set the flag and break the loop
						 (dbl "short circuited" i j e)
						 ;; Return false
						 (return-from element-checking
									  nil))))
		   ;; Must've passed all tests
		   t)))

(defaliases mtx-equal mtx= mat=)

;;; Testing

(defun TEST-mults ()
  (blk "testing multiplies")
  (let ((A (make-matrix 3 2 2))
		(B (make-matrix 2 4 3)))
	(setf (mat-elt A 1 1) 4)
	(blk "A")
	(show-matrix A)
	(blk "B")
	(show-matrix B)
	(blk "A*B")
	(show-matrix (mat* A B))
	(let ((B (make-matrix 3 2 1)))
	  (blk "A+B")
	  (show-matrix (add-2-matrices B A)) )))

(defun TEST2 ()
  (let ((M (make-matrix 2 3 69)))
	(block-format "testing accessors..")
	(show-matrix M)
	(br)
	(block-format "by index")
	(domatrix-by-index (a b M)
					   (format t "~a ~a = ~a ~%" a b (mtx-elt M a b)))
	(block-format "by element")
	(domatrix (i j e M)
			  (format t "~a,~a = ~a~%" i j e))))

(defun TEST-eq ()
  (blk "testing mat=")
  (let ((A (make-matrix 2 2 2)))
	(dbl (mat= A A))
	(dbl (mat= A (make-mat-copy A)))
	(dbl (mat= A (make-matrix 2 2 3)))
	(dbl (mat= A (make-matrix 1 1 1)))))

(defun TEST-adding ()
  (blk "testing matrix adds")
  (let ((A (make-matrix 2 2 0))
		(B (make-matrix 2 2 2))
		(C (make-matrix 2 2 3)))
	(show-matrix (mat+ A B C))))
'(TEST)
'(TEST2)
'(TEST-adding)
'(TEST-eq)
