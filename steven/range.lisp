(in-package :an.steven.range)

'(defmacro range (a b)
   "Like Python's range function.  Returns Integers [a, b-1]."
   (if (and (constantp a)
			(constantp b))
	 ;; If a and b are both constants, build the list at compile time.
	 (let ((out (loop for i from (eval a) to (- (eval b) 1) collect i)))
	   (format t "building list compile time~%")
	   (push 'list out)
	   out)
	 ;; Otherwise, output the code to do it
	 `(loop for i from ,a to (- ,b 1) collect i)))

(defmacro range (a b)
  "Like Python's range function.  Returns Integers [a, b-1]."
  (let ((out `(loop for i from ,a to (- ,b 1) collect i)))
	(if (and (constantp a) (constantp b))
	  ;; If a and b are both constants, eval the list at compile time.
	  (progn
		(format t "eval'ing list compile time~%")
		;; Return `(list 1 2 ... 4)
		(cons 'list (eval out)))
	  ;; Otherwise, just output the uneval'd code)
	  ;; Return `(loop for i ... collect i)
	  out)))

;; Testing
'(defun TEST-range ()
  (load "debug.lisp")
  (block-format "RANGE tests")
  (defconstant +A+ 10)
  (defconstant +B+ 20)
  (dbl "testing constants"
	   (macroexpand-1 '(range 6 9))
	   (macroexpand-1 '(range (+ 3 3) (+ 3 3 3)))
	   (macroexpand-1 '(range (1+ +A+) (1+ +B+))))

  (let ((a 6)
		(b 9))
	(dbl "testing local vars"
		 (macroexpand-1 '(range a b))
		 (macroexpand-1 '(range (1+ a) (1+ b))) )))
