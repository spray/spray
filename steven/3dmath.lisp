(in-package :an.steven.3dmath)

;;; TODO - can i not define each func three times..like not have three v-*'s?  Use macros somehow?

;; Vector 3D
(defconstant +num-vector-dimensions+ 3)

(defun make-vector (x y z)
  (vector x y z))
(defmacro v-x (v) `(aref ,v 0))
(defmacro v-y (v) `(aref ,v 1))
(defmacro v-z (v) `(aref ,v 2))

;; Ops

(defun make-vector-s (s)
  (make-vector s s s))

(defmacro do-xyz ((&rest pairs) &body body-forms)
  `(do-vector-elements-known-size
	 ,+num-vector-dimensions+
	 ,pairs
	 ,@body-forms))

'(defmacro do-xyz ((&rest pairs) &body body-forms)
  "For each given symbol/vector pair, this binds the symbol to all 3
  components of the vector, while runnng the body-forms for each component."
  ;; First, generate a unique symbol to bind to each vector.
  ;; This is so we don't evaluate them more than once.
  (let ((vector-syms (loop for pair in pairs collect (gensym))))
	;; Output: We want to evaluate each vector form into the above symbols
	`(let (,@(loop for pair in pairs
				   for sym in vector-syms
				   collect `(,sym ,(second pair))))
	   ;; Output: Now, we want a symbol macrolet for each component, and also to run
	   ;; the body form.
	   ,@(loop for i from 0 to (1- +num-vector-dimensions+) collect
			   ;; For each sym/vector pair, macro-bind the sym to the vector's component.
			   ;; But remember to use the vector symbols bound above
			   `(symbol-macrolet
				  (,@(loop for pair in pairs
						   for sym in vector-syms
						   collect `(,(first pair) (aref ,sym ,i))))
				  ,@body-forms)))))

;; BRIEFLY TESTED
(defun dot-product (a b)
  (+ (* (v-x a) (v-x b))
	 (* (v-y a) (v-y b))
	 (* (v-z a) (v-z b))))

;; BRIEFLY TESTED
(defun cross-product (a b)
  (make-vector
	(- (* (v-y a) (v-z b))
	   (* (v-z a) (v-y b)))
	(- (* (v-z a) (v-x b))
	   (* (v-x a) (v-z b)))
	(- (* (v-x a) (v-y b))
	   (* (v-y a) (v-x b)))))
(defun ncross-product (a b)
  (setf (v-x a)	(- (* (v-y a) (v-z b))
				   (* (v-z a) (v-y b))))
  (setf (v-y a) (- (* (v-z a) (v-x b))
				   (* (v-x a) (v-z b))))
  (setf (v-z a)	(- (* (v-x a) (v-y b))
				   (* (v-y a) (v-x b))))
  a)


(defun ^2 (x) (* x x))
(defun neg (x) (* -1 x))

(defun magnitude (v)
  (sqrt (+ (^2 (v-x v))
		   (^2 (v-y v))
		   (^2 (v-z v)))))

(defun v+ (a b)
  (make-vector
	(+ (v-x a) (v-x b))
	(+ (v-y a) (v-y b))
	(+ (v-z a) (v-z b))))
(defun nv+ (a b)
	(setf (v-x a) (+ (v-x a) (v-x b)))
	(setf (v-y a) (+ (v-y a) (v-y b)))
	(setf (v-z a) (+ (v-z a) (v-z b)))
	a)

(defun v- (a b)
  (make-vector
	(- (v-x a) (v-x b))
	(- (v-y a) (v-y b))
	(- (v-z a) (v-z b))))
(defun nv- (a b)
	(setf (v-x a) (- (v-x a) (v-x b)))
	(setf (v-y a) (- (v-y a) (v-y b)))
	(setf (v-z a) (- (v-z a) (v-z b)))
	a)

(defun v* (v s)
  "Returns v scaled by s"
  (make-vector
	(* (v-x v) s)
	(* (v-y v) s)
	(* (v-z v) s)))
(defun nv* (v s)
  (setf (v-x v) (* (v-x v) s))
  (setf (v-y v) (* (v-y v) s))
  (setf (v-z v) (* (v-z v) s))
  v)

(defun vflip (v)
  (v* v -1))

(defun comp* (a b)
  (make-vector
	(* (v-x a) (v-x b))
	(* (v-y a) (v-y b))
	(* (v-z a) (v-z b))))
(defun ncomp* (a b)
  (setf (v-x a) (* (v-x a) (v-x b)))
  (setf (v-y a) (* (v-y a) (v-y b)))
  (setf (v-z a) (* (v-z a) (v-z b)))
  a)

(defun v/ (v s)
  "Returns v scaled by 1/s"
  ; (declare (number s))
  (v* v (/ 1 s)))
(defun nv/ (v s)
  "Returns v scaled by 1/s"
  ; (declare (number s))
  (nv* v (/ 1 s)))

(defun normalized (v)
  (v/ v (magnitude v)))
(defun normalize-f (v)
  "This actually modifies v, instead of just returning ||v||."
  ;; This can be re-written cleaner
  (let ((mag (magnitude v)))
	(setf (v-x v) (/ (v-x v) mag))
	(setf (v-y v) (/ (v-y v) mag))
	(setf (v-z v) (/ (v-z v) mag))
	v))
 
;; Helper functions

(defun dir-from (a b)
  "Returns a normalized direction vector from a to b."
  (let ((dir (v- b a)))
	(normalize-f dir)))

(defun distance (a b)
  (magnitude (v- a b)))

(defun cos-between (a b)
  (/ (dot-product a b)
	 (* (magnitude a)
		(magnitude b))))

(defconstant +rads-per-degs+ (/ pi 180))
(defconstant +degs-per-rads+ (/ 180 pi))
(defun degs->rads (d)
  (* d +rads-per-degs+))
(defun rads->degs (r)
  (* r +degs-per-rads+))

(defconstant +half-pi+ (/ pi 2))

(defun ~= (a b &optional (eps 1e-6))
  (and (< a (+ b eps))
	   (> a (- b eps))))

(defun angle-between (a b)
  "In radians"
  (acos (cos-between a b)))

(defun at-most-1 (x)
  (if (> x 1.0) 1.0
	x))
(defun angle-between-normals (n1 n2)
  "In radians"
  (acos (at-most-1
		  (dot-product n1 n2))))

;; Ray structure
;; TODO - try actually using (defstruct) for this.  Would that be faster for CMUCL?

(defun make-ray (origin dir)
  (vector origin dir))
(defun r-origin (ray)
  (aref ray 0))
(defun r-dir (ray)
  (aref ray 1))

(defun copy-ray (orig)
  (make-ray (copy-seq (r-origin orig))
	    (copy-seq (r-dir orig))))

;; Ray operations

(defun point-on-ray (ray t-val)
  ; (declare (number t-val))
  (v+ (r-origin ray)
	  (v* (r-dir ray) t-val)))

(defun make-ray-from-line (a b)
  (make-ray
	a	; From the first point..
	(normalized (v- b a))))	; Use the normalized a->b vector as the direction

;; Geometry

;; spheres

(defun ray-sphere-intersection-t-vals (ray sphere-origin sphere-radius)
  ; (declare (vector sphere-origin))
  ; (declare (number sphere-radius))
  "Returns t such that r(t) is a point on the sphere.  t may be negative!  If there is no intersection, it returns nil."
  ;; TODO - add declares to these let's
  (let* ((rd (r-dir ray))
		 (ro (r-origin ray))
		 (sc sphere-origin)
		 (Xd (v-x rd)) (Yd (v-y rd)) (Zd (v-z rd))
		 (Xo (v-x ro)) (Yo (v-y ro)) (Zo (v-z ro))
		 (Xc (v-x sc)) (Yc (v-y sc)) (Zc (v-z sc))
		 (B (* 2 (+ (* Xd (- Xo Xc))
					(* Yd (- Yo Yc))
					(* Zd (- Zo Zc)))))
		 (C (+ (^2 (- Xo Xc))
			   (^2 (- Yo Yc))
			   (^2 (- Zo Zc))
			   (neg (^2 sphere-radius))))
		 ;; The discriminant
		 (disc (- (^2 B) (* 4 C))))
	; (declare (number Xd Yd Zd
					 ; Xo Yo Zo
					 ; Xc Yc Zc
					 ; B C
					 ; disc))
	; (declare (vector rd ro sc))

	(cond ((< disc 0.0)
		   ;; No intersection
		   nil)
		  (t
			;; Calculate intersection - two roots
			(let* ((sqrt-disc (sqrt disc))
				   (ta (- (neg B) sqrt-disc))
				   (tb (+ (neg B) sqrt-disc)))
			  ; (declare (number num0 num1))
			  ;; If ta is non-negative, return it (it's always smaller than tb)
			  ;; Of course, return it halved.. we delayed the /2a part of the eqn..
			  ;(if (> ta tb) (break))
			  ;(dbl ta tb)
			  (if (= ta tb) nil	;; Consider tangential hits no hit
				(if (>= ta 0.0)
				  ;; TA is non-neg, so both must be non-neg.  Return both halved
				  (list (* 0.5 ta) (* 0.5 tb))
				  ;; Well, is tb non-neg?  If so, just return that
				  (if (>= tb 0.0)
					(list (* 0.5 tb))
					;; Both are negative!  No hits.
					'())))
			  )))))

(defun sphere-normal-at (center radius pt)
  ; (declare (number radius))
  (normalized
	(make-vector
	  (/ (- (v-x pt) (v-x center)) radius)
	  (/ (- (v-y pt) (v-y center)) radius)
	  (/ (- (v-z pt) (v-z center)) radius))))

;;;;;;;;;;;;;;;;;;;;
;; TEST
;;;;;;;;;;;;;;;;;;;;

'(defun TEST ()
   (let ((x (make-vector (random 10) (random 10) (random 10)))
		 (y (make-vector (random 10) (random 10) (random 10)))
		 (z (make-vector (random 10) (random 10) (random 10))))
	 (dbl x y (cross-product x y))
	 (dbl (dot-product x (cross-product x y)))
	 (dbl (dot-product y (cross-product x y)))
	 (let ((c (cross-product x y)))
	   (dbl c (/ (dot-product c (normalized c)) (magnitude c))))
	 (let ((ray (make-ray-from-line x y))
		   (point)
		   (o-p))
	   (setf point (point-on-ray ray 2.0))
	   (dbl (magnitude (v- point (r-origin ray))))
	   (setf o-p (v- point (r-origin ray)))
	   (dbl (/ (dot-product o-p (v- y x))
			   (* (magnitude o-p) (magnitude (v- y x)))))
	   )
	 ))

'(defun TEST-sphere ()
   (blk "sphere test")
   (let ((origin (make-vector 0 0 0))
		 (sphere-center (make-vector -1 -1 0))
		 (start (make-vector 1 1 0))
		 sphere)
	 (setf sphere (make-sphere sphere-center 1))
	 (let ((ray (make-ray-from-line start origin))
		   (intx))
	   (setf intx (ray-sphere-intersection ray sphere))
	   (dbl intx (sphere-normal-at sphere intx)))))

'(TEST-sphere)
