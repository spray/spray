(in-package :an.steven.utils)

(defun self-evaluating-p (form)
  (and (atom form) (if (symbolp form) (keywordp form) t)))

(defmacro defalias-less-headache-version (alias actual)
  "Creates a function/macro alias name.
  For example:
  (defalias myplus +)
  (myplus 4 5) -> 9
  This took me a LONG TIME to get right.."
  `(defmacro ,alias (&rest args)
					; This turns out like, (list 'actual ,@args)
    (list* (quote ,actual) args)))

;; Trying to be more slick with this version
;; Both should work the same
(defmacro defalias (alias actual)
  "
  Creates a function/macro alias name.
  For example:
  (defalias myplus +)
  (myplus 4 5) -> 9
  This took me a LONG TIME to get right..
  Especially the ,', part...geezus.
  "
  `(defmacro ,alias (&rest args)
					; This turns out like, (list 'actual ,@args)
    `(,',actual ,@args)))

(defmacro defaliases (actual &rest aliases)
  "Creates many aliases for actual.  This probably should be used sparingly.."
  (let ((out (list)))
    (dolist (alias aliases)
      (push `(defalias ,alias ,actual) out))
    (push 'progn out)
    out))

(defun most (fn lst)
  "This is equivalent to (most2 fn list #'>)"
  (if (null lst)
      (values nil nil)
      (let* ((wins (car lst))
	     (max (funcall fn wins)))
	(dolist (obj (cdr lst))
	  (let ((score (funcall fn obj)))
	    (when (> score max)
	      (setq wins obj
		    max score))))
	(values wins max))))

(defun least (fn lst)
  (most #'(lambda (elt)
	    (* -1 (funcall fn elt)))
	lst))

(defun find-best (fn lst comp-fn)
  "This works like 'most', but you get to provide your own comparison function.  If a is better than b, then (comp-fn a b) should return t.  It then returns the pair (fn elt) such that (fn elt) wins."
  (if (null lst)
      (values nil nil)
      (let* ((max (funcall fn (car lst))))
	(dolist (obj (cdr lst))
	  (let ((score (funcall fn obj)))
	    (when (funcall comp-fn score max)
	      (setq max score))))
	max)))

(defmacro nil? (x) `(eq ,x nil))
(defmacro non-nil? (x) `(not (eq ,x nil)))

(defmacro lazily-evaluated (place-form eval-form)
  "Example: (lazily-evaluated *sum* (+ 1 10))"
  `(progn
    (when (nil? ,place-form)
      (setf ,place-form ,eval-form))
    ,place-form))

(defmacro setf-if-nil (place-form eval-form)
  (lazily-evaluated place-form eval-form))

(defmacro if-not-nil (form if-nil-form)
  `(cond ((nil? ,form) ,if-nil-form)
    (t ,form))) 

(defmacro nil-if-nil (form if-not-nil-form)
  "If form evaluates to nil, nil is returned.  Otherwise, if-not-nil-form is evaluated and returned."
  `(cond ((nil? ,form) nil)
    (t ,if-not-nil-form)))

(defmacro if-nil (form then else)
  `(if (nil? ,form) ,then
    ,else))

;; Um..I think (case ...) does the same thing as this..
(defmacro switch (value-form case-forms default-form)
  (let ((value-sym (gensym))
	(cond-cases '()))
    ;; Generate all the cond-cases
    ;; Like, ((eq value-sym case-value))
    (dolist (case-form (nreverse case-forms))
      (let ((case-value (first case-form))
	    (case-eval-forms (cdr case-form)))
	(push `((equal ,value-sym ,case-value)
		,@case-eval-forms) cond-cases)))
    ;; Now build the overall let, with the optional default
    `(let ((,value-sym ,value-form))
      (cond ,@cond-cases
	    (t ,default-form)))))
;; EXAMPLE
'(switch answer
  ((2 (alert "two!"))
   (3 (alert "three!")))
  (alert "unknown"))

;; TODO - macro list-to-vars

;; Time stuff
(defun internal-time->secs (time-units)
  "Given a time amount in interval time units (as returned by get-universal-time), this converts it into seconds."
  (/ time-units INTERNAL-TIME-UNITS-PER-SECOND)	)
'(defun get-internal-secs ()
  "Major precision issues - DO NOT USE"
  (float (internal-time->secs (get-internal-real-time))))

;; Heart beating
(defclass heartbeater ()
  ((period-secs :initarg :period-secs
		:initform 1
		:reader period-secs)
   (last-beat-secs :initform (get-universal-time)
		   :accessor last-beat-secs)))

(defmethod try-beat ((beater heartbeater) beat-fn)
  (let ((last-beat-secs (last-beat-secs beater))
	(period-secs (period-secs beater))
	(now-secs (get-universal-time)))
    ;; Has enough time passed?
    (when (>= (- now-secs last-beat-secs) period-secs)
      ;; ..Yes
      ;; Beat!
      (funcall beat-fn)	
      ;; Record the time of the beat
      (setf (last-beat-secs beater) now-secs))))

(defmacro do-heartbeat ((beater) &body beat-forms)
  "If the beater is due to beat, then beat-forms will be executed and the beater will be updated.  Otherwise, nothing will happen.  This form does not evaluate to anything in particular"
  `(let ((now-secs (get-universal-time)))
    (when (>= (- now-secs (last-beat-secs beater))
	      (period-secs beater))
      (progn ,@beat-forms)
      (setf (last-beat-secs beater) now-secs))))

(defun TEST-heartbeat ()
  (let ((beater (make-instance 'heartbeater))
	(x 0))
    (loop while (< x 10) do
	  (do-heartbeat (beater)
	    (format t "~a.." x)
	    (incf x)))))

;; Floating point and numbers

(defmacro zero-if-underflow (expr)
  `(handler-case ,expr
    (floating-point-underflow () 0)))
(defmacro zero-if-overflow (expr)
  `(handler-case ,expr
    (floating-point-overflow () 0)))

;; We use vectors to represent colors, until they get sent to imago.  Then we need these functions to convert.
(defun clamp-to (x min max)
  (cond ((< x min) min)
	((> x max) max)
	(t x)))

;; CLOS utils
;; These actually don't work at all.  The defclass macro gets access to (public-field ...) forms
;; and tries to interpret them itself, before macro expansion gets to it.
;; Use the defclass-java-style macro instead
(defmacro fields-with-readers (&rest forms)
  "Takes a list of lists and augments each sub-list with :reader field"
  (let ((out ()))
    (dolist (field-form forms)
      (let ((field-name (first field-form)))
	(push `(,field-name :reader ,field-name) out)))
    out))

(defmacro public-field (field-name initarg initform &rest extras)
  `(,field-name :initform ,initform
    :initarg ,initarg
    :accessor ,field-name
    ,@extras))
(defmacro readonly-field (field-name initarg initform &rest extras)
  `(,field-name :initform ,initform
    :initarg ,initarg
    :reader ,field-name
    ,@extras))

;; Consecutive enumerations
;; Example: (defenum READY DATA DONE) --> (defconstant READY 0) (defconstant DATA 1) (defconstant DONE 2)
;; TODO
					;(defmacro defenum )

(defun TEST ()
  (block-format "begin utils testing")
  (load "debug.lisp")
  
  (defalias myplus +)
  (br)
  (dbl "should be 6" (myplus 1 2 3))
  
  (dbl (macroexpand-1 '(defalias p format)))
  
  (defalias blk block-format)
  (blk "IT WORKS!! 69: ~a" 69))


