(in-package :an.steven.vector)

;; Random utilities for vectors - not the 3D graphics kind.

(defmacro last-index (v) `(1- (length ,v)))

(defun vec-equal (a b)
  (if (/= (length a) (length b))
	;; Length's aren't even equal..
	;; Return false
	nil )

  ;; Length's are equal
  ;; Check the elements
  (loop for i from 0 to (last-index a) do
		(if (/= (elt a i) (elt b i))
		  ;; For a non-equal element
		  ;; Return false
		  nil))
  ;; Passed all tests
  t)

;; Vector macros
(defmacro do-vector-elements-known-size (size (&rest pairs) &body body)
  "This unrolls the loop across the vector at compile-time."
  (when (not (constantp size))
	(error "** do-vector-elements-known-size: size was not constant!"))
  ;; First, generate a unique symbol to bind to each vector.
  ;; This is so we don't evaluate them more than once.
  (let ((vector-syms (loop for pair in pairs collect (gensym))))
	;; Output: We want to evaluate each vector form into the above symbols
	`(let (,@(loop for pair in pairs
				   for sym in vector-syms
				   collect `(,sym ,(second pair))))
	   ;; Output: Now, we want a symbol macrolet for each component, and also to run
	   ;; the body form.
	   ,@(loop for i from 0 to (1- size) collect
			   ;; For each sym/vector pair, macro-bind the sym to the vector's component.
			   ;; But remember to use the vector symbols bound above
			   `(symbol-macrolet
				  (,@(loop for pair in pairs
						   for sym in vector-syms
						   collect `(,(first pair) (aref ,sym ,i))))
				  ,@body)))))

