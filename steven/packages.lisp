(in-package :cl-user)

;;; Define my packages

(defpackage :an.steven.utils
  (:use :common-lisp)
  (:export
	:self-evaluating-p
	:defalias
	:defaliases
	:most
	:find-best
	:try-beat
	:heartbeater
	:zero-if-underflow
	:zero-if-overflow
	:clamp-to
	:public-field
	:nil?
	:non-nil?
	:lazily-evaluated
	:setf-if-nil
	:if-not-nil
	:nil-if-nil
	:switch
	))

(defpackage :an.steven.clos
  (:use :common-lisp
		:an.steven.utils)
  (:export
	:defclass-java-style
	:define-typical-print-method
	:define-formatting-print-method
	))

(defpackage :an.steven.debug
  (:use :common-lisp
		:an.steven.utils)
  (:export
	:defalias
	:br
	:hr
	:dbl
	:dbl-if
	:dbmsg
	:block-format
	:blk
	:debug-progn
	:dbp
	:with-trace
	:monitor-progn
	:show-value-smartly-and-return
	:dbv
	:assert-value
	:*assert-value-enabled*
	))

(defpackage :an.steven.range
  (:use :common-lisp
		:an.steven.debug
		:an.steven.utils)
  (:export 
	:range))

(defpackage :an.steven.vector
  (:use :common-lisp)
  (:export
	:last-index
	:vec-equal
	:do-vector-elements-known-size))

(defpackage :an.steven.matrix
  (:use :common-lisp
		:an.steven.utils
		:an.steven.debug
		:an.steven.vector)
  (:export
	:make-matrix
	:num-rows
	:num-cols
	:last-row
	:last-col
	:mtx-elt :mat-elt
	:domatrix-by-index
	:domatrix
	:domatrix-rows-by-index
	:domatrix-rows
	:show-matrix
	:mtx* :mat*
	:mtx-equal :mat= :mtx=))

(defpackage :an.steven.3dmath
  (:use :cl
		:an.steven.vector
		:an.steven.utils
		:an.steven.debug)
  (:export
	:make-vector
	:do-xyz
	:v-x :v-y :v-z
	:dot-product :cross-product
	:magnitude
	:v+ :v- :v* :v/
	:nv+ :nv- :nv* :nv/
	:vflip
	:comp*
	:ncomp*
	:normalized
	:normalize-f
	:make-ray
	:r-origin
	:r-dir
	:copy-ray
	:point-on-ray
	:make-ray-from-line
	:dir-from
	:distance
	:ray-sphere-intersection
	:ray-sphere-intersection-t-vals
	:sphere-normal-at
	:make-sphere :s-center :s-radius
	:^2
	:neg
	:cos-between
	:angle-between
	:angle-between-normals
	:~=
	:+half-pi+
	)
  )

(defpackage :an.steven.raytracer1
  (:use :cl
		:imago
		:an.steven.utils
		:an.steven.debug
		:an.steven.3dmath)
  (:export
	:phong-shade
	:light-calc
	:vec->color
	))
;; NOTE: You may have to delete all FAS files and re-asdf-load
