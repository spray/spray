(in-package :an.steven.debug)

(defvar *debug-output* *trace-output*)	; true will just send it to std-out

;;; Printing and logging

(defmacro newline () `(format *debug-output* "~%"))
(defalias br newline)
(defmacro hr () `(format *debug-output* "~%----------------~%"))
(defmacro block-format (format-string &rest args)
  `(progn 
	 (hr)
	 (format *debug-output* ,format-string ,@args)
	 (hr)))
(defalias blk block-format)

;;; Convenient printing tricks

(export 'show-form-and-value-l)
(defmacro show-form-and-value-l (form)
  (let ((value-var (gensym)))
	`(let ((,value-var ,form))
	   (format *debug-output* "~a -> ~a~%"  ',form ,form)
	   ,value-var)))
(defalias dbv show-form-and-value-l)
(defmacro show-form-and-value (form)
  `(format *debug-output* "~a -> ~a" ',form ,form) )
(defmacro show-value (form)
  `(format *debug-output* "~a" ,form) )
(defmacro show-self-evaluating (form)
  ;; TODO - this one should take form, get its evaluation, then build a string out of it - instead of using ~a
  `(format *debug-output* "~a" ,form) )
(defmacro show-self-evaluating-l (form)
  ;; TODO - this one should take form, get its evaluation, then build a string out of it - instead of using ~a
  `(format *debug-output* "~a~%" ,form) )
(defmacro show-form-smartly (form)
  "If form is self-evaluating, it just shows the value.  Otherwise, it shows the form and the value."
  (cond ((self-evaluating-p form)
		 `(show-self-evaluating ,form))
		(t
		  `(show-form-and-value ,form) )))

(defmacro show-form-smartly-l (form)
  "If form is self-evaluating, it just shows the value.  Otherwise, it shows the form and the value."
  (cond ((self-evaluating-p form)
		 `(show-self-evaluating-l ,form))
		(t
		  `(show-form-and-value-l ,form) )))

(defmacro debug-sentence (&rest parts)
  "
  <<<<
  Print a list of forms in a smart manner: If it's self-evaluating (like a string or number), just print it directly.  If it's a form (like (+ 1 2)), print the form and evaluation (so it would become (+ 1 2) = 3).
  >>>>
  "
  (let ((out (list)))
	(dolist (part (reverse parts))
	  (push `(format t "; ") out)
	  (push `(show-form-smartly ,part) out)
	)
	(push 'progn out)
	out ))
(defalias dbmsg debug-sentence)
(defmacro dbl (&rest parts)
  "
  <<<<
  Print a line to *debug-output*.
  Ex: (dbl \"Let's do some math..\" (+ 1 2) (* 2 3) (sqrt 2))
  This would yield: Let's do some math.. (+ 1 2) = 3 (* 2 3) = 6 (sqrt 2) = 1.4..
  So it's useful for debugging and tracing variables.
  >>>>
  "
  `(progn
	 (dbmsg ,@parts)
	 (format t "~%")
	 ))

(defmacro show-smartly-and-return (form)
  `(let ((result ,form))
	 (format *debug-output* "~a -> ~a~%" ',form result)
	 result))

(defmacro debug-progn (&rest forms)
  (let ((out '()))
	;; Evaluate and return the last form
	(push `(show-smartly-and-return ,(car (last forms)))
		  out)
	(dolist (form (reverse (butlast forms)))
	  (push `(show-form-smartly-l ,form) out))
	(push 'progn out)))
(defalias dbp debug-progn)

(defmacro monitor-progn (mtr-vars &rest forms)
  (let ((out '()))
	;; Evaluate and return the last form
	(push `(show-smartly-and-return ,(car (last forms)))
		  out)
	(dolist (form (reverse (butlast forms)))
	  (push `(dbl ,@mtr-vars)
			out)
	  (push `(format *debug-output* "    monitor: ")
			out)
	  (push `(show-form-smartly-l ,form) out))
	(push 'progn out)))

(defmacro dbl-if (cond-exp &rest forms)
  `(when ,cond-exp (dbl ,@forms)))

;; Value assertions
(defvar *assert-value-enabled* t)
(defmacro assert-value (value-form value-sym assert-form)
  "This will evaluate value-form once and bind value-sym to the resulting value.
  Then, it evaluates assert-form - if that returns t, then value-sym's value will be returned.
  If it assert-form evaluates to nil, a detailed error will be signaled."
  (cond (*assert-value-enabled*
		  `(let ((,value-sym ,value-form))
			 (when (nil? ,assert-form)
			   (dbl "*** assert-value failed!  value-form" ',value-form "evaluated to" ,value-sym "assert-form" ',assert-form)
			   (error "Assertion failed!  See above"))
			 ,value-sym))
		;; Asserts are disabled
		;; Just return the value-form
		(t value-form)))

(defmacro with-trace ((&rest funcs) &body body)
  "Evaluates to the given body as usual, but traces the given functions.  Then, it untraces then after the body is finished evaluating.  NOTE: If a given function name was already being traced, it WILL be untraced outside of this form.  This is somewhat undesirable..and it may be changed in the future."
  (let ((body-result-sym (gensym)))
    `(progn
      (trace ,@funcs)
      (let ((,body-result-sym (progn ,@body)))
	(untrace ,@funcs)
	,body-result-sym))))

;; Test code
(defun TEST-with-trace ()
  (progn
    (defun foo (x) 'foo)
    (defun bar (x) 'bar)
    (with-trace (foo bar)
      (foo 1) (bar 2))))

;;; Testings

(defun TEST-debug ()
  (block-format "begin testing")

  (show-form-and-value (+ 1 2))
  (br)
  (show-form-and-value 69)
  (br)
  (show-form-smartly 69)
  (br)
  (show-form-smartly (+ 1 2))
  (br)

  (dbl "end of tests" (+ 1 2 3) "we're done!")

  (dbl (defvar x 1))
  (dbl (debug-progn (incf x) (incf x) (incf x)))
  (dbl (progn (decf x) (decf x)))

  (blk "testing monitor progn")
  (defvar y 2)
  (monitor-progn (x y)
				 (decf x)
				 (incf y)
				 (incf x)
				 (incf y)
				 (incf x)
				 (decf x))

  (blk "done")
  (let ((seven 7))
	(dbl "ok done for real now" seven) ))
