
(asdf:defsystem
  steven
  :author "Steven An"
  :version "1.0"
  :components
  ((:file "packages")
   (:file "utils" :depends-on ("packages"))
   (:file "debug" :depends-on ("utils"))
   (:file "range" :depends-on ("debug"))
   (:file "vector" :depends-on ("packages"))
   (:file "matrix" :depends-on ("debug" "vector"))
   (:file "3dmath" :depends-on ("debug"))
   (:file "raytracer" :depends-on ("3dmath"))
   (:file "clos" :depends-on ("utils"))
   )
  :depends-on
  (; :test-framework
	:imago)
  )

