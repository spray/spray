(in-package :an.steven.raytracer1)

(defvar *depth-limit* 2)

;; DEBUGGING 
(defvar *debug-pixels-example* (list '(29 38)
									 '(28 38)
									 '(27 38)
									 '(30 38)))
(defvar *debug-pixels* ())

(defvar *db-on* nil)
(defun start-debugging () (setf *db-on* t))
(defun stop-debugging () (setf *db-on* nil))
(defun is-debug-pixel? (x y)
  (let ((out nil))
	(dolist (db-pixel *debug-pixels*)
	  (when (and (= x (first db-pixel))
				 (= y (second db-pixel)))
		(setf out t)))
	out))

(defmacro my-dbl (&rest forms)
  `(dbl-if *db-on* ,@forms))

(defun debug-if-pixel-is-interesting (x y)
  (when (is-debug-pixel? x y)
	(blk "===")
	(dbl "turning on debugging for pixel" x y)
	(start-debugging)))

;; Views
(defclass raytracing-view-base ()
  ())
(defgeneric view-origin (view))
(defgeneric get-viewplane-point (view u v))
(defgeneric get-viewplane-ray (view u v))

(defmethod get-viewplane-ray ((view raytracing-view-base) u v)
  "NOTE: This returns a ray from the view origin that goes through the given viewplane point."
  (make-ray-from-line
	(view-origin view)
	(get-viewplane-point view u v)))

;; A very simple, static view.  Mainly for debugging.
(defclass simple-RT-view (raytracing-view-base)
  ())
(defmethod view-origin ((view simple-RT-view))
  (make-vector 0 0 5))	; Looking at the origin
(defmethod get-viewplane-point ((view simple-RT-view)
								u v)
  "Simulate a 10x10 viewing plane, so [-1,1]->[-5,5]"
  (make-vector (* u 5)
			   (* v 5)
			   0))

;; Perspective view - TODO low priority
(defclass perspective-view (raytracing-view-base)
  (origin dir fov bl-corner tl-corner br-corner))
(defmethod view-origin ((view perspective-view))
  (slot-value view 'origin))
(defmethod get-viewplane-point ((view perspective-view) u v)
  (list u v)
  ;; TODO
  )
'(defmethod initialize-instance ((view perspective-view))
   ;; TODO - calculate the corner points
   )

(defun calc-UV-for-pixel (width height x y)
  "This returns the coordinates of (x,y) in the following coordinate system defined for the given the screen (width/height): The middle of the screen is 0,0 and 1,1 is the top-right corner, and -1,-1 is the bottom left corner."
  (let ((half-x (/ width 2))
		(half-y (/ height 2)))
	(let ((trans-x (- x half-x))
		  (trans-y (- y half-y)))
	  ;; UV coords are normalized, so divide the translated coords by the "radius" of the screen
	  (values (/ trans-x half-x)
			  (/ trans-y half-y)))))

(defun calc-pixel-ray (width height x y view)
  (multiple-value-bind (u v) (calc-UV-for-pixel width height x y)
	(get-viewplane-ray view u v)))

;; lighting
(defclass light-base ()
  ())
(defgeneric illuminate-hit (light hit view))

(defclass point-light (light-base)
  ((origin :initarg :origin
		   :reader origin)
   (color :initarg :color
		  :reader color)
   (fall-off :initarg :fall-off
			 :initform 100
			 :reader fall-off)))

(defmethod illuminate-hit ((light point-light) hit view)
  "Phong shades the hit point."
  (phong-shade-hit light hit view))

(defun shadow-ray-blocked (surf-pt light-pt scene hit-obj)
  "Shoots a ray from surface to light and see if it hits anything in between."
  (my-dbl "shadow-ray-blocked" hit-obj)
  (let ((rayhit (trace-ray scene
						   (shadow-ray surf-pt light-pt)
						   hit-obj)))
	(cond (rayhit ;; There was a hit.  Hmm..was it closer than the light?
			(cond ((< (hit-dist rayhit) (distance surf-pt light-pt))
				   ;; The hit was closer than the light!
				   ;; Return true - it was blocked
				   t)
				  ;; Otherwise..we're OK.  Return false.
				  (t nil)))
		  ;; No hit - nothing is blocking
		  (t nil))))

;; geometry elements
(defclass geometry-object () ())
(defgeneric ray-intersection-t-val (geom ray))
(defgeneric surface-normal (geom pt))

;; sphere-object
(defclass sphere-object (geometry-object)
  ((center :initarg :center
		   :reader center)
   (radius :initarg :radius
		   :reader radius)))
(defmethod ray-intersection-t-val ((geom sphere-object) ray)
  (ray-sphere-intersection-t-val ray (center geom) (radius geom)))
(defmethod surface-normal ((geom sphere-object) pt)
  (sphere-normal-at (center geom) (radius geom) pt))
(defun make-sphere (center radius)
  (make-instance 'sphere-object :center center :radius radius))

;; axis-aligned box
(defclass axis-aligned-box-object (geometry-object)
  ((negative-corner :initarg :negative-corner
					:reader negative-corner)
   (dimensions :initarg :dimensions
			   :reader dimensions)))
'(defmethod ray-intersection-t-val ((geom axis-aligned-box-object) ray)
  (ray-AABB-intersection-t-val ray
							   (negative-corner geom)
							   (dimensions geom)))
'(defmethod surface-normal ((geom axis-aligned-box-object) pt)
  (TODO))
(defun make-AABB (neg-corner dimensions)
  (make-instance 'axis-aligned-box-object
				 :negative-corner neg-corner
				 :dimensions dimensions))

(defclass infplane-object (geometry-object)
  ((normal :initarg normal
		   :reader normal)
   ;; The shortest distance to the origin
   (dist :initarg dist
		 :reader dist)))
(defmethod ray-intersection-t-val ((geom infplane-object) ray)
  (list ray)
  ;; TODO
  nil)
(defmethod surface-normal ((geom infplane-object) pt)
  (list pt)	;; Shut up warning
  (normal geom))

;; Ray trace hits
;; TODO - make this a defstruct
(defun make-ray-hit (pt norm dist mat &optional (obj nil)) (vector pt norm dist mat obj))
(defun hit-point (h) (aref h 0))
(defun hit-normal (h) (aref h 1))
(defun hit-dist (h) (aref h 2))
(defun hit-mat (h) (aref h 3))
(defun hit-obj (h) (aref h 4))

(defun hit-normal-ray (h) (make-ray (hit-point h) (hit-normal h)))
(defun shadow-ray (surf-pt light-origin)
  (make-ray-from-line surf-pt light-origin))

;; Raytracing math/algorithms

;; TEMP - these are not realy used at all..
(defvar *background-color* (make-vector 1 1 1))
(defvar *ambient-light-color* (make-vector 0.1 0.1 0.1))

(defun hit-to-view-ray (hit view)
  (v- (view-origin view)
	  (hit-point hit)))

(defun bounced-ray (hit incoming)
  "Returns the ray that starts from the hit point and goes in the direction that the incoming ray would reflect off at."
  (let ((neg-incoming-dir (v* (r-dir incoming) -1)))
	(make-ray
	  (hit-point hit)
	  (reflected-dir neg-incoming-dir (hit-normal hit)))))

(defvar *dbv*)

(defun raytrace-recursively-using-tail-recursion-2 (scene view ray depths-left to-ignore
														  &optional (contrib-so-far (make-vector 0 0 0)))
  "Functional tail recursion."
  (cond ((<= depths-left 0)
		 ;; Reached depth limit
		 ;; Don't contribute anything new
		 ;; Return the contrib so far
		 contrib-so-far)
		;; Otherwise, shoot the ray and recurse
		(t
		  '(my-dbl "shooting a contribution ray......")
		  ;; Trace this ray and add contributions from reflected rays
		  (let ((hit (trace-ray scene ray to-ignore)))
			(cond ((eql hit nil)
				   ;; We hit nothing - we'll make no contribution.
				   ;; Just return what we have so far.
				   contrib-so-far)
				  ;; We got a hit!
				  (t
					'(dbl "recursing")
					;; Now recurse
					(raytrace-recursively-using-tail-recursion-2
					  scene view
					  (bounced-ray hit ray)	; Now shoot the bounced ray
					  (1- depths-left)	; Duh
					  (hit-obj hit)	; Now ignore the object we just hit
					  ;; Add the contribution from this hit
					  (nv+ (nv* (illuminate-surface-point scene view hit)
							  (reflection-coeff (hit-mat hit)))
						  contrib-so-far))))))))

;; Choose the default raytracing mechanism here
(defun raytrace-recursively (scene view ray depths-left &optional (to-ignore nil))
  (raytrace-recursively-using-tail-recursion-2 scene view ray depths-left to-ignore))

(defun sum-of-colors (colors)
  (cond ((eql nil colors) (make-vector 0 0 0))
		(t (v+ (car colors) (sum-of-colors (cdr colors))))))

(defun average-of-colors (colors)
  '(dbl colors)
  (cond ((eql colors nil) (make-vector 0 0 0))
		(t (nv/ (sum-of-colors colors)
			   (length colors)))))

(defun do-grid (cx cy half-edge half-divs fn)
  (cond ((= half-divs 0) (funcall fn cx cy))
		(t
		  (let ((sx (- cx half-edge))
		(sy (- cy half-edge))
		(dx (/ half-edge half-divs))
		(dy (/ half-edge half-divs))
		(num-divs (* 2 half-divs)))
			;; Calculate points
			(loop for ix from 0 to num-divs do
				  (loop for iy from 0 to num-divs collect
						(funcall fn
								 (+ sx (* ix dx))
								 (+ sy (* iy dy)))))))))

(defvar *super-sampling-level* 0)

(defun raytrace-sample (x y w h scene view)
  (let ((ray (calc-pixel-ray w h x y view)))	; Calculate the ray to shoot
	(raytrace-recursively scene view ray *depth-limit*)))

(defun raytrace-pixel (x y w h scene view)
  (let ((samples '()))
	(do-grid x y 0.5 *super-sampling-level*
			 #'(lambda (x y)
				 ;; Shoot the ray for this grid pt
				 ;; and add the color to the samples list
				 (push (raytrace-sample x y w h scene view) samples)))
	;; Now average samples and return
	(average-of-colors samples)))

(defun raytrace-img (img view scene)
  (let ((w (image-width img))
		(h (image-height img))
		y
		color
		(beater (make-instance 'heartbeater :period-secs 1)))
	(dbl "raytracing image" w h)
	;; Ray tracing loop
	(do-image-pixels (img pix x flipped-y)
					 (try-beat beater #'(lambda () (dbl "at pixel" x y)))
					 (debug-if-pixel-is-interesting x flipped-y)
					 (setf y (- h flipped-y))
					 '(dbl "raytracing pixel" x y)
					 (setf color (raytrace-pixel x y w h scene view))
					 '(dbl "setting pixel")
					 (my-dbl "final color" color)
					 (stop-debugging)
					 (setf pix (vec->color color))
					 '(dbl "set pixel"))))	; Set the pixel

;; Utilities
(defun make-image (width height)
  (make-instance 'rgb-image
				 :width width
				 :height height))

(defmacro round* (a b)
  `(round (* ,a ,b)))

(defmacro draw-pt (img x y color)
  `(imago::draw-point ,img ,x ,y ,color))

;; Scenes
(defclass raytrace-scene-base () ())
(defgeneric trace-ray (scene ray &optional to-ignore))
(defgeneric trace-ray-t-val (scene ray &optional to-ignore))
(defgeneric get-lights (scene))
(defgeneric illuminate-surface-point (scene view hit
											&optional lights contrib-so-far))

(defmethod trace-ray ((scene raytrace-scene-base) ray &optional to-ignore)
  (multiple-value-bind (hit-obj t-val mat) (trace-ray-t-val scene ray to-ignore)
	(cond (hit-obj
			;; Got a hit - create a hit-obj
			(let ((hit-pt (point-on-ray ray t-val)))
			  (make-ray-hit hit-pt
							(surface-normal hit-obj hit-pt)
							t-val
							mat
							hit-obj)))
		  (t nil))))

;; Materials
(defclass material-base ()
  ())
(defclass solid-material (material-base)
  ((diffuse-color :initarg :diffuse-color
				  :reader diffuse-color)
   (specular-color :initarg :specular-color
				   :reader specular-color)
   (specular-exponent :initarg :specular-exponent
					  :reader specular-exponent)
   (reflection-coeff :initarg :reflection-coeff
					 :initform 0.5
					 :reader reflection-coeff)))

;; Atom scenes
;; Atom scenes should be concave (so they CAN'T self-shadow, thus the ignoring is valid)
;; Atom scenes have obj-ray intersection functions.
(defclass single-geom-scene (raytrace-scene-base)
  ((geom :initarg :geom
		 :initform (error "no object given")
		 :reader geom)
   (material :initarg :material
			 :initform (error "no material")
			 :reader material)))

(defmethod trace-ray-t-val ((scene single-geom-scene) ray &optional to-ignore)
  (let ((geom (geom scene)))
	(cond ((eql geom to-ignore) nil)	;; Return no hit if we're ignored
		  (t (let ((t-val (ray-intersection-t-val geom ray)))
			   (cond
				 ((and t-val (> t-val 0.0))
				  ;; t-val exists and is positive - we got a good hit
				  ;; Return the hit geom and the t-val
				  ;; and the material
				  (values geom t-val (material scene)))
				 ;; Otherwise, return no hit
				 (t nil)))))))

(defun make-sphere-scene (center radius material)
  (make-instance 'single-geom-scene
				 :geom (make-sphere center radius)
				 :material material))

;;; Composed scene - made up of many other scenes
(defclass composed-scene (raytrace-scene-base)
  ((scenes :initarg :scenes
		   :accessor scenes)
   (lights :initarg :lights
		   :initform '()
		   :accessor lights)))
(defmethod trace-ray-t-val ((scene composed-scene) ray &optional to-ignore)
  (let ((hit-cons
		  ;; Get the scene that yields the closest hit
		  ;; This uses a simple linear scan to find the best hit
		  (find-best #'(lambda (scene)
						 (multiple-value-bind (obj t-val mat) (trace-ray-t-val scene ray to-ignore)
						   (list obj t-val mat)))
					 ;; find best of all scenes..
					 (scenes scene)
					 ;; closer means better
					 #'(lambda (a b)
						 ;(declare (fixnum a b))
						 (let ((t-a (second a))
							   (t-b (second b)))
						   (cond ((eql t-b nil) t)
								 ((eql t-a nil) nil)
								 (t (< t-a t-b))))))))
	;; Now we have the best hit
	;; Return it
	(values (first hit-cons) (second hit-cons) (third hit-cons))))

;;; Infinite planes
(defconstant +positive-x-normal+ (make-vector 1 0 0))
(defconstant +positive-y-normal+ (make-vector 0 1 0))
(defconstant +positive-z-normal+ (make-vector 0 0 1))
(defclass infplane-scene (raytrace-scene-base)
  ())
(defmethod trace-ray ((scene infplane-scene) ray &optional to-ignore)
  (list to-ignore)	;; shut up warning
  (let ((s (r-origin ray))
		(d (r-dir ray)))
	(cond ((= (v-z d) 0.0) nil)	; Ray is parallel to XY-plane - no hit
		  (t
			;; Solve for t when z=0
			;; Basically, s.z + t*d.z = 0
			;; --> t = (5-s.z)/d.z
			'(dbl (v-z s) (v-z d))
			(let ((t-val (/ (- 0 (v-z s)) (v-z d))))
			  ;; Calculate the hitpt
			  (let ((hitpt (point-on-ray ray t-val)))
				;; Make and return the hit
				(make-ray-hit hitpt
							  +positive-z-normal+	; normal
							  t-val
							  nil)))))))	; No material
;;; Boxes

;;; Lights

(defvar *temp-pt-lights*
  (list (make-instance 'point-light
					   :origin (make-vector 1 1 5)
					   :color (make-vector 0.8 0.3 0.3))
		(make-instance 'point-light
					   :origin (make-vector -1 -1 5)
					   :color (make-vector 0.3 0.3 0.8))))

(defconstant +no-illumination-color+ (make-vector 0 0 0))
(defmethod illuminate-surface-point ((scene composed-scene) view hit
															&optional
															(lights *temp-pt-lights*)
															(contrib-so-far +no-illumination-color+))
  (let ((light (car lights)))
	(cond ((eql light nil)
		   ;; No lights to do.  Return what we got so far.
		   contrib-so-far)
		  (t
			;; Recursively add up contributions from lights
			(illuminate-surface-point
			  scene view hit (cdr lights)	;; Send everything and the rest of the lights
			  ;; Now calculate the current light's contribution plus the contribution so far..
			  (nv+ contrib-so-far
				  ;; Is the light blocked?
				  (cond ((shadow-ray-blocked (hit-point hit) (origin light) scene (hit-obj hit))
						 ;; It's blocked!
						 ;; No contrib
						 +no-illumination-color+)
						(t
						  ;; Calculate the contribution
						  (illuminate-hit light hit view)))))))))


;; A test scene
;; A simple XY-plane

(defvar *width* 200)
(defvar *height* 200)

'(defmethod illuminate-with-all-lights ((scene composed-scene) view hit)
  ;; Shoot shadow rays while ignoring
  (cond ((shadow-ray-blocked
		   (hit-point hit)
		   (origin *temp-pt-light*)
		   scene
		   (hit-obj hit))
		 ;; It's blocked!  No light.
		 ;; Use the ambient light
		 (ncomp* *ambient-light-color*
				(diffuse-color (hit-mat hit))))
		(t
		  ;; Light it!
		  (illuminate-hit *temp-pt-light*
						  hit view)
		  )))

;; shading
(defun reflected-dir (to-reflect reflect-by)
  "Given a direction to the light and a surface normal, this returns the light vector reflected along the surface normal."
  '(dbl "r-d" to-reflect reflect-by)
  (nv- (v* reflect-by
		   (* 2 (dot-product to-reflect reflect-by)))
	   to-reflect))

'(defun angle-between-normals (n1 n2)
  (acos (dot-product n1 n2)))

(defun reflected-ray (to-reflect reflect-by)
  '(dbl "r-r" to-reflect reflect-by)
  (let ((refl-dir (reflected-dir (r-dir to-reflect)
								 (r-dir reflect-by))))
	'(dbl "returning ray")
	(make-ray (r-origin to-reflect) refl-dir)))

;; shading math

(defun light-calc (diffuse specular N.L V.R spec-exp)
  '(dbl "light-calc" diffuse specular N.L V.R spec-exp)
  (+ (* diffuse N.L)
	 (* specular
		(zero-if-underflow
		  (expt V.R spec-exp)))))

;; Some optics math

(defun at-least-0 (x)
  "If x < 0.0, returns 0.0.  Otherwise, returns x."
  (if (< x 0.0) 0.0 x))

(defun phong-shade (light-origin light-color fall-off
								 hit-pt hit-norm to-viewer-dir
								 diffuse specular spec-exp)
  (let* ((contrib (make-vector 0 0 0))
		 (to-light-dir (dir-from hit-pt light-origin))
		 (N.L (dot-product hit-norm to-light-dir)))
	(if (<= N.L 0.0)
	  ;; The surface doesn't even face the light
	  ;; Return no contribution
	  contrib
	  ;; Otherwise, let there be light
	  (let* ((refl-dir (reflected-dir to-light-dir hit-norm))
			 (V.R (at-least-0 (dot-product to-viewer-dir refl-dir)))
			 (dist-to-light (distance hit-pt light-origin)))
		;; Calculate lighting with speculars
		(setf (v-x contrib) (light-calc (v-x diffuse) (v-x specular) N.L V.R spec-exp))
		(setf (v-y contrib) (light-calc (v-y diffuse) (v-y specular) N.L V.R spec-exp))
		(setf (v-z contrib) (light-calc (v-z diffuse) (v-z specular) N.L V.R spec-exp))
		;; Contribute the light color
		;; and scale for fall-off
		(let ((fall-off-factor (cond ((< dist-to-light fall-off) 1.0)
									 (t (/ fall-off dist-to-light)))))
		  (setf (v-x contrib) (* (v-x contrib) (v-x light-color) fall-off-factor))
		  (setf (v-y contrib) (* (v-y contrib) (v-y light-color) fall-off-factor))
		  (setf (v-z contrib) (* (v-z contrib) (v-z light-color) fall-off-factor)))
		;; Return the calculated contribution
		contrib))))

(defun phong-shade-hit (light hit view)
  (let ((mat (hit-mat hit)))
	(phong-shade
	  (origin light) (color light) (fall-off light)	; the light
	  (hit-point hit) (hit-normal hit)	; the hit
	  (dir-from (hit-point hit) (view-origin view))
	  (diffuse-color mat) (specular-color mat) (specular-exponent mat))))

;; My imago helpers
(defmacro do-rgb ((var color) &rest forms)
  (let ((out ()))
	(dolist (rgb-func (list 'color-red 'color-green 'color-blue))
	  (push `(let ((,var (,rgb-func ,color)))
			   ,forms)
			out))
	(push 'progn out)
	out))

(defmacro do-rgb-fn ((rgb-fn) &rest forms)
  `(progn
	 (let ((,rgb-fn #'color-red))
	   ,@forms)
	 (let ((,rgb-fn #'color-green))
	   ,@forms)
	 (let ((,rgb-fn #'color-blue))
	   ,@forms)))

(defun clear-img (img &optional (color +white+))
  (do-image-pixels (img pix x y)
				   (setf pix color)))

;; We use vectors to represent colors, until they get sent to imago.  Then we need these functions to convert.

(defun vec->color-component (c)
  "Maps [0,1] linearly to [0,255] U Integers.  Rounds with the 'round' function."
  '(dbl c)
  (round (* (clamp-to c 0.0 1.0) 255)))


(defun vec->color (v)
  "Returns the 32-bit imago color based on vector v.  v is assumed to be normalized, meaning each component is in [0,1].  This map is effectively [0,1]->[0,256].  The 'round' function is used to round.  And (x,y,z)->(r,g,b)."
  (make-color (vec->color-component (v-x v))
			  (vec->color-component (v-y v))
			  (vec->color-component (v-z v))))

;;; Common scenario functions
(defvar *common-view* (make-instance 'simple-RT-view))
(defun raytrace-scene-to-png (scene
							   png-file
							   width
							   height
							   &key (view *common-view*)
							   (skip-writing nil))
  (let ((img (make-image width height)))
	(raytrace-img img view scene)
	(unless skip-writing
	  (dbl "writing " png-file "..")
	  (write-png img png-file))))

;;;;;;;;;;;;;;;;;;;;
;; UNIT TESTS
;;;;;;;;;;;;;;;;;;;;

;; TODO - ok, the pattern shown by the test below is too tall. I think i'm scaling the Y axis wrong somewhere in my pixel-ray calcs.  Whatever - just keep the image a square.
(defun draw-line-vec (img a b clr)
  (draw-line img
			 (v-x a) (v-y a)
			 (v-x b) (v-y b)
			 clr))

;; Iso projection, for testing
(defun draw-projected-ray (img ray &optional (color +Black+))
  (let ((start (projected-pt (r-origin ray)))
		(end (projected-pt (point-on-ray ray 2.5))))
	'(dbl start end)
	(draw-line-vec img start end color)))

(defun projected-pt (pt)
  (make-vector
	(+ (floor *width* 2) (round* 10 (v-x pt)))
	(- (floor *height* 2) (round* 10 (v-y pt)))	;; Y is inverted in imago
	0))

;;; Some sample scenes

(defvar *test-img* (make-image *width* *height*))
(defvar *test-view* (make-instance 'simple-RT-view))

(defvar *shiney-green-mat*
  (make-instance 'solid-material
				 :diffuse-color (make-vector 0 1 0)
				 :specular-color (make-vector 1 0 1)
				 :specular-exponent 2
				 :reflection-coeff 1))
(defvar *shiney-blue-mat*
  (make-instance 'solid-material
				 :diffuse-color (make-vector 0 0 1)
				 :specular-color (make-vector 1 1 0)
				 :specular-exponent 5
				 :reflection-coeff 1))
(defvar *shiney-red-mat*
  (make-instance 'solid-material
				 :diffuse-color (make-vector 1 0 0)
				 :specular-color (make-vector 0 1 1)
				 :specular-exponent 1
				 :reflection-coeff 1))
(defvar *shiney-yellow-mat*
  (make-instance 'solid-material
				 :diffuse-color (make-vector 1 1 0)
				 :specular-color (make-vector 0 0 1)
				 :specular-exponent 1
				 :reflection-coeff 1))
(defvar *shiney-white-mat*
  (make-instance 'solid-material
				 :diffuse-color (make-vector 1 1 1)
				 :specular-color (make-vector 1 1 1)
				 :specular-exponent 3
				 :reflection-coeff 1))
(defvar *diffuse-red-mat*
  (make-instance 'solid-material
				 :diffuse-color (make-vector 1 0 0)
				 :specular-color (make-vector 0 0 0)
				 :specular-exponent 0
				 :reflection-coeff 0.1))

(defvar *three-sphere-scene*
  (make-instance 'composed-scene
				 :scenes (list (make-sphere-scene (make-vector 0 2 0) 2 *shiney-green-mat*)
							   (make-sphere-scene (make-vector 2 -1 0) 1 *shiney-blue-mat*)
							   (make-sphere-scene (make-vector -1 -2 0) 1 *diffuse-red-mat*)
							   (make-sphere-scene (make-vector 0 0 -1050) 1000 *shiney-white-mat*))))
(defvar *two-sphere-scene*
  (make-instance 'composed-scene
				 :scenes (list (make-sphere-scene (make-vector 0 2 0) 2 *shiney-green-mat*)
							   (make-sphere-scene (make-vector 1 -2 0) 1 *shiney-blue-mat*)
							   (make-sphere-scene (make-vector 0 0 -1050) 1000 *shiney-white-mat*))))

(defun TEST-composed-scene (&optional (skip-writing nil))
  (blk "testing composed scene")
  (let ((scene *two-sphere-scene*))
	(time (raytrace-scene-to-png scene
								 "rt-composed-2.png"
								 *width* *height*
								 :skip-writing skip-writing))))

(defun TEST-refract ()
  (blk "TEST-refract")
  (let ((in-dir (normalized (make-vector -1 -1 0)))
		(surf-norm +positive-y-normal+)
		(n-coeff 1))
	(let ((trans-dir (refracted-dir in-dir surf-norm n-coeff)))
	  (dbl in-dir surf-norm n-coeff trans-dir))))
'(TEST-refract)

'(TEST-composed-scene)

(defalias TEST TEST-composed-scene)

;; DONE:
;;;;;;;;;;;;;;;;;
;; BUG: look at the infplane test..there seems to be a ring around the highlight.  It should be smoother!  This is definitely a problem with the specular calcs.
;; 11-06-2005 18:20:04 - figured it out.  It's maxing out the green at the ring, thus the ring.  But then it gets brighter by using the other componenets, R/B...thus it seems like there's an artifical plateau step.  But really, it's just the green getting maxed out.  So if the specular color has no green in it, it looks smooth!  This will be fixed when I implement the exponential exposure color modulation.

;; DONE:
;;;;;;;;;;;;;;;;;
;; BUG - I think object ignoring in trace-ray is still not working..we still get artifacts
;; 14-06-2005 09:48:14 The ignoring is working correctly I think.  What's interesting is that these only occur on "virtual edges".  See "bug rt-....png"
;; 18-06-2005 10:12:33 Bug fixed..I wasn't ignoring negative t-values properly.  Solving it involving tracing the code (essentially, step through with a debugger, but automatically) and then plotting hit points/rays in Maya.

;; NEXT:
;; 22-06-2005 01:51:16 NEXT: refractions!  make material have indices of refraction (for the inside), and use lenz's law
;; 28-06-2005 02:52:49 the light calc expt call causes underflows if the exponent is too large - handle it and just return 0 or something
