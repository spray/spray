;; Load this file (after load-first) to load and run the raytracer

(blk "loading raytracer")
(asdf-load t 'raytracer)
(in-package :an.steven.raytracer)

(blk "running raytracer functional test case")
(setf *trace-indent* t)

;; Redirect trace output to an open file
(with-open-file (*trace-output*
		 "trace-out.txt"
		 :direction :output
		 :if-exists :supersede)
  (time (identity (rt-test))))
